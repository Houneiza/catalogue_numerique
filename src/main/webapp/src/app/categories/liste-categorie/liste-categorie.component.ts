import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CategorieService } from 'src/app/services/categorie.service';
import { first } from 'rxjs/operators';
import { CategorieInput } from 'src/app/models/categorieInput';

@Component({
  selector: 'app-liste-categorie',
  templateUrl: './liste-categorie.component.html',
  styleUrls: ['./liste-categorie.component.css']
})
export class ListeCategorieComponent implements OnInit {
  categorieForm: FormGroup;
  submitted = false;
  categorieInput: CategorieInput;
  error = '';

  search;
  page: Number = 1; 
  totalrecords: string;
  catId: number;
  name: string;
  category:[];

  categorie = [
  
    {
      nom: 'INFORMATIQUE',
    },
    {
      nom: 'AGEP',
    },
    {
      nom: 'GCA',
    },
  ]
  constructor(private route: Router, 
              private fb: FormBuilder,
              private router: ActivatedRoute,
              private categorieService: CategorieService) { }

  ngOnInit(): void {
    this.categorieForm = this.fb.group({
      name: ['', Validators.required],
    });

    this.getAll();
  }

  get f(){
    return this.categorieForm.controls;
  } 

  create(){
    this.submitted = true;

    if(this.categorieForm.invalid){
      return;
    }

    var name = this.categorieForm.controls['name'].value;

    this.categorieInput = new CategorieInput(
                          null,
                          name,
                        );

    this.categorieService.create(this.categorieInput)
    .subscribe(
      data =>{
        console.log(data);
        this.route.navigate(['categories/liste-categorie']);
        this.getAll();
      },
      error =>{
        this.error = error;
        console.log(error);
      }
    )
  }
  
  getAll(){
    this.categorieService.getAll()
    .subscribe(
      data =>{
        this.category = data.content;
        console.log(data.content);
      },
      error =>{
        this.error = error;
        console.log(error);
        
      }
    )
  }

  findById(){
    this.catId = parseInt(this.router.snapshot.paramMap.get('id'));
    console.log(this.catId);
    this.categorieService.findById(this.catId)
    .pipe(first())
    .subscribe(
      data => {
        this.name = data.content.name;
        console.log(data.content);
      },
      error => {
        this.error = error;
        console.log(error);
        
      }
    )
  }

  delete(id){
    //this.catId = parseInt(this.router.snapshot.paramMap.get('id'));
    //console.log(this.catId);
    this.categorieService.delete(id)
    .pipe(first())
    .subscribe(
      data => {
        this.getAll();
      },
      error => {
        this.error = error;
        console.log(error);
      }
    )
  }


}
