import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategorieService } from 'src/app/services/categorie.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CategorieInput } from 'src/app/models/categorieInput';

@Component({
  selector: 'app-creer-categorie',
  templateUrl: './creer-categorie.component.html',
  styleUrls: ['./creer-categorie.component.css']
})
export class CreerCategorieComponent implements OnInit {

  categorieForm: FormGroup;
  submitted = false;
  categorieInput: CategorieInput;
  error = '';

  constructor(
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.categorieForm = this.fb.group({
      nom: ['', Validators.required],
    })
  }

}
