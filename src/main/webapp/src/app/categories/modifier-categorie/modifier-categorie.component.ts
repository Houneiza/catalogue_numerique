import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CategorieService } from 'src/app/services/categorie.service';
import { CategorieInput } from 'src/app/models/categorieInput';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Categorie } from 'src/app/models/categorie';

@Component({
  selector: 'app-modifier-categorie',
  templateUrl: './modifier-categorie.component.html',
  styleUrls: ['./modifier-categorie.component.css']
})
export class ModifierCategorieComponent implements OnInit {

  categorieForm: FormGroup;
  submitted = false;
  error : '';
  name: FormControl;
  categorieInput: CategorieInput;
  categorie: Categorie;
  categorieId: number;

  constructor(private route: Router,
              private router: ActivatedRoute,
              private categorieService: CategorieService,
              ) { }

  ngOnInit(): void {
    this.categorieId = parseInt(this.router.snapshot.paramMap.get('id'));
    this.name = new FormControl('', Validators.required);
    this.categorieForm = new FormGroup({
      name: this.name,
    });

    this.categorieService.findById(this.categorieId)
    .subscribe(
      data =>{
        console.log(data);
        this.name.setValue(data.body.name);
        
        
      },
      error =>{
        this.error = error;
        console.log(error);
        
      }
    )
  }

  get fcategorie(){
    return this.categorieForm.controls;
  }

  update(){
     this.submitted = true;
     if(this.categorieForm.invalid){
       return;
     }

     this.categorieInput = new CategorieInput(
                      this.categorieId,
                      this.name.value,
                    );
                  
    this.categorieService.update(this.categorieInput)
    .subscribe(
      data => {
      this.route.navigate(['categories/liste-categorie']);
    },
    error => {
      this.error = error;
      console.log(error);
    })

  }

}
