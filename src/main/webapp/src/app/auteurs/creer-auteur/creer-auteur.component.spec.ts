import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreerAuteurComponent } from './creer-auteur.component';

describe('CreerAuteurComponent', () => {
  let component: CreerAuteurComponent;
  let fixture: ComponentFixture<CreerAuteurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreerAuteurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreerAuteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
