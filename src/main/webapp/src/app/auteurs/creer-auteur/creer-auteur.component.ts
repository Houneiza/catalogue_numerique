import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Auteur } from 'src/app/models/auteur';
import { AuteurInput } from '../../models/auteurInput';
import { AuteurService } from '../../services/auteur.service';

@Component({
  selector: 'app-creer-auteur',
  templateUrl: './creer-auteur.component.html',
  styleUrls: ['./creer-auteur.component.css']
})
export class CreerAuteurComponent implements OnInit {

  auteurForm: FormGroup;
  submitted = false;
  auteurs: Auteur[]= [];
  auteurInput: AuteurInput; 
  error = '';

  constructor(private route: Router, 
              private fb: FormBuilder,
              private auteurService: AuteurService) { 
               
  }

  ngOnInit(): void {/* 
    this.auteurForm = this.fb.group({
      nom: ['', Validators.required],
      //prenomAuteur: ['', Validators.required]
    }); */
  }



}
