import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerAuteurComponent } from './supprimer-auteur.component';

describe('SupprimerAuteurComponent', () => {
  let component: SupprimerAuteurComponent;
  let fixture: ComponentFixture<SupprimerAuteurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerAuteurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerAuteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
