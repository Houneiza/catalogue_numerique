import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuteurService } from 'src/app/services/auteur.service';
import { AuteurInput } from 'src/app/models/auteurInput';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-modifier-auteur',
  templateUrl: './modifier-auteur.component.html',
  styleUrls: ['./modifier-auteur.component.css']
})
export class ModifierAuteurComponent implements OnInit {

  auteurForm: FormGroup;
  submitted = false;
  auteurId: number;
  nom: FormControl;
  error = '';
  auteurInput: AuteurInput;

  constructor(private route: Router,
              private fb: FormBuilder,
              private router: ActivatedRoute,
              private auteurService: AuteurService) { }

  ngOnInit(): void {
    this.auteurId = parseInt(this.router.snapshot.paramMap.get('id'));
    console.log(this.auteurId);
    this.nom = new FormControl('', Validators.compose([Validators.required, Validators.minLength(4)])),
    this.auteurForm = new FormGroup({
      nom: this.nom,
      //prenomAuteur: ['', Validators.required]
    });

    this.auteurService.findById(this.auteurId)
    .subscribe(
      data =>{
        console.log(data);
        this.nom.setValue(data.body.nom);
      },
      error =>{
        this.error = error;
        console.log(error);
        
      }
    )
  }

  get fauteur(){
    return this.auteurForm.controls;
  }

  update(){
    this.submitted = true;
    if(this.auteurForm.invalid){
      return;
    }

    this.auteurInput = new AuteurInput(
                        this.auteurId,
                        this.nom.value,
                      )

    console.log(this.auteurInput);
    this.auteurService.update(this.auteurInput)
    .subscribe(
      data =>{
        console.log(data);
        this.route.navigate(['auteurs/liste-auteur']);
      },
      error =>{
        this.error = error;
        console.log(error);
      }
    )
  }

}
