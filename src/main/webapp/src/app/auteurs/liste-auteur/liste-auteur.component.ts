import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Auteur } from 'src/app/models/auteur';
import { Router, ActivatedRoute } from '@angular/router';
import { AuteurService } from 'src/app/services/auteur.service';
import { first } from 'rxjs/operators';
import { AuteurInput } from 'src/app/models/auteurInput';

@Component({
  selector: 'app-liste-auteur',
  templateUrl: './liste-auteur.component.html',
  styleUrls: ['./liste-auteur.component.css']
})
export class ListeAuteurComponent implements OnInit {

  auteurForm: FormGroup;
  submitted = false;

  search;
  page: Number = 1; 
  totalrecords: string;
  author: [];
  error = '';
  auteurId : number;
  nom: string;
  auteurInput: AuteurInput;
  auteur = [
  
    {
      nom: 'fomo',
    },
    {
      nom: 'Douwe',
    },
    {
      nom: 'aboubou',
    },
    {
      nom: 'Houneiza', 
    },
    {
      nom: 'menga', 
    }
    
  ]

  constructor(private route: Router, 
              private fb: FormBuilder,
              private auteurService: AuteurService,
              private router: ActivatedRoute) { }

  ngOnInit(): void {
    this.auteurForm = this.fb.group({
      nom: ['', Validators.required],
      //prenomAuteur: ['', Validators.required]
    });
    this.getAll();
    this.findById();
  }

  get f(){
    return this.auteurForm.controls;
  } 

  create(){
    this.submitted = true;

    if(this.auteurForm.invalid){
      return;
    }

    var nom = this.auteurForm.controls['nom'].value;

    this.auteurInput = new AuteurInput(
                      null,
                      nom,
                    );
    console.log(this.auteurInput);
    
    this.auteurService.create(this.auteurInput)
    .subscribe(
      data =>{
        console.log(data);
        this.getAll();
        //this.route.navigate(['auteurs/liste-auteur']);
      },
      error =>{
        this.error = error;
        console.log(this.error);
        
      }
    )
  }
  
  getAll(){
    this.auteurService.getAll()
    .subscribe(
      data =>{
        this.author = data.content;
        console.log(data.content);
      },
      error =>{
        this.error = error;
        console.log(error);
        
      }
    )
  }

  findById(){
    this.auteurId = parseInt(this.router.snapshot.paramMap.get('id'));
    console.log(this.auteurId);
    this.auteurService.findById(this.auteurId)
    .pipe(first())
    .subscribe(
      data => {
        this.nom = data.content.nom;
        console.log(data.content);
      },
      error => {
        this.error = error;
        console.log(error);
        
      }
    )
  }

  delete(id){
    //this.auteurId = parseInt(this.router.snapshot.paramMap.get('id'));
    //console.log(this.router.snapshot.paramMap.get('id'));
    
    console.log(id);
    this.auteurService.delete(id)
    .pipe(first())
    .subscribe(
      data => {
        this.getAll();
      },
      error => {
        this.error = error;
        console.log(error);
      }
    )
  }

}
