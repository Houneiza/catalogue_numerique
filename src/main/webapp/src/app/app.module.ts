import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { CreerAuteurComponent } from './auteurs/creer-auteur/creer-auteur.component';
import { ListeAuteurComponent } from './auteurs/liste-auteur/liste-auteur.component';
import { ModifierAuteurComponent } from './auteurs/modifier-auteur/modifier-auteur.component';
import { CreerCategorieComponent } from './categories/creer-categorie/creer-categorie.component';
import { ListeCategorieComponent } from './categories/liste-categorie/liste-categorie.component';
import { ModifierCategorieComponent } from './categories/modifier-categorie/modifier-categorie.component';
import { CreerLivreComponent } from './livres/creer-livre/creer-livre.component';
import { ModifierLivreComponent } from './livres/modifier-livre/modifier-livre.component';
import { ListeLivreComponent } from './livres/liste-livre/liste-livre.component';
import { HomeComponent } from './home/home.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Ng2SearchPipeModule} from 'ng2-search-filter';
import { NgxPaginationModule} from 'ngx-pagination';
import { SupprimerAuteurComponent } from './auteurs/supprimer-auteur/supprimer-auteur.component';
import { SupprimerCategorieComponent } from './categories/supprimer-categorie/supprimer-categorie.component';
import { SupprimerLivreComponent } from './livres/supprimer-livre/supprimer-livre.component';
import { ListeLivresComponent } from './categories/liste-livres/liste-livres.component';
import { BibliothequeComponent } from './bibliotheque/bibliotheque.component';
import { CreerUtilisateurComponent } from './utilisateurs/creer-utilisateur/creer-utilisateur.component';
import { ListeUtilisateurComponent } from './utilisateurs/liste-utilisateur/liste-utilisateur.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CreerAuteurComponent,
    ListeAuteurComponent,
    ModifierAuteurComponent,
    CreerCategorieComponent,
    ListeCategorieComponent,
    ModifierCategorieComponent,
    CreerLivreComponent,
    ModifierLivreComponent,
    ListeLivreComponent,
    HomeComponent,
    SupprimerAuteurComponent,
    SupprimerCategorieComponent,
    SupprimerLivreComponent,
    ListeLivresComponent,
    BibliothequeComponent,
    CreerUtilisateurComponent,
    ListeUtilisateurComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
