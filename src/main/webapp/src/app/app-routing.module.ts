import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreerAuteurComponent } from './auteurs/creer-auteur/creer-auteur.component';
import { LoginComponent } from './login/login.component';
import { CreerLivreComponent } from './livres/creer-livre/creer-livre.component';
import { ListeLivreComponent } from './livres/liste-livre/liste-livre.component';
import { ModifierLivreComponent } from './livres/modifier-livre/modifier-livre.component';
import { CreerCategorieComponent } from './categories/creer-categorie/creer-categorie.component';
import { ListeCategorieComponent } from './categories/liste-categorie/liste-categorie.component';
import { ModifierCategorieComponent } from './categories/modifier-categorie/modifier-categorie.component';
import { ListeAuteurComponent } from './auteurs/liste-auteur/liste-auteur.component';
import { ModifierAuteurComponent } from './auteurs/modifier-auteur/modifier-auteur.component';
import { HomeComponent } from './home/home.component';
import { AppComponent } from './app.component';
import {BibliothequeComponent} from './bibliotheque/bibliotheque.component';
import { SupprimerCategorieComponent } from './categories/supprimer-categorie/supprimer-categorie.component';
import { SupprimerLivreComponent } from './livres/supprimer-livre/supprimer-livre.component';
import { SupprimerAuteurComponent } from './auteurs/supprimer-auteur/supprimer-auteur.component';

const routes: Routes = [
  {path: "", component: LoginComponent, pathMatch: 'full'},
  {path: "home", component: HomeComponent, canActivate: []},
  {path: "auteurs/creer-auteur", component: CreerAuteurComponent},
  {path: "auteurs/modifier-auteur/:id", component: ModifierAuteurComponent},
  {path: "auteurs/liste-auteur", component: ListeAuteurComponent},
  {path: "auteurs/supprimer-auteur", component: SupprimerAuteurComponent},
  {path: "livres/creer-livre", component: CreerLivreComponent},
  {path: "livres/modifier-livre/:id", component: ModifierLivreComponent},
  {path: "livres/liste-livre", component: ListeLivreComponent},
  {path: "livres/supprimer-livre", component: SupprimerLivreComponent},
  {path: "categories/creer-categorie", component: CreerCategorieComponent},
  {path: "categories/modifier-categorie/:id", component: ModifierCategorieComponent},
  {path: "categories/liste-categorie", component: ListeCategorieComponent},
  {path: "categories/supprimer-categorie", component: SupprimerCategorieComponent},
  {path:"bibliotheque", component:BibliothequeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
