import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

declare const $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  @Output() gotoHome = new EventEmitter();
  @Output() gotobiblio = new EventEmitter();

  currentMessage: String;
  thread: any;
  search;

  /* La formation et la recherche dans les domaines suivants : Agriculture, Elevage et Produits ... */

  messages: string[] = [
    'L’ENSPM (Ex Institut Supérieur du Sahel) de l’Université de Maroua est un pôle d’Excellence polytechnique au service du développement local, national et sous-regional.',
    'Elle a pour missions spécifiques d’assurer : La formation et la recherche dans les domaines suivants : Agriculture, Elevage et Produits Dérivés , Energies Renouvelables , Sciences Environnementales, Génie Civil et Architecture, Informatique et Télécommunications, Génie Textile et Cuir, Hydraulique et Maîtrise des Eaux, Météorologie, Climatologie, Hydrologie et Pédologie.',
    'La bibliothèque de l’ENSPM est ouverte à tous les étudiants et dispose des livres de multitudes de domaines parmi lesquel: information, agriculture, élévage et produits, droit humanitaire, mathématiques, genie chimique, technologie textile et design, journaux et revue, etc...',
    'De ce fait, via ce catalogue, vous pouvez vous renseigner sur la présence d’un livre au sein de la bibliothèque ou que vous soyez',
  ];


  constructor(private route: Router) {
    
   }

  ngOnInit(): void {
    this.initMessageAnimation();
  }

  disConnect(){
    this.route.navigate(['/']);
    this.gotoHome.emit();
  }

  biblio(){
    this.gotobiblio.emit();
    this.route.navigate(['/bibliotheque']);
  }

  initMessageAnimation(): void{
    console.log('init animation')
    let messageIndex = 0;
    this.currentMessage = this.messages[messageIndex]
    let self = this;
    setTimeout(() => self.thread = setInterval(function(){
        self.currentMessage = self.messages[++messageIndex%4]
      }, 5000), 3000)
    clearInterval(this.thread);
    $('.carousel-control-prev').change(function(){
      console.log('Change')
    }).click(function(){
      console.log('click')
    })
  }
}
