import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { LivreService } from '../services/livre.service';

@Component({
  selector: 'app-bibliotheque',
  templateUrl: './bibliotheque.component.html',
  styleUrls: ['./bibliotheque.component.css']
})
export class BibliothequeComponent implements OnInit {
  
  @Output() gotoreturn = new EventEmitter();
  @Output() gotobiblio = new EventEmitter();

  book: [];
  error: '';
  search;
  page: Number = 1; 
  totalrecords: string;

  constructor(private route : Router,
              private livreService: LivreService) { }

  ngOnInit(): void {
    this.getAll();
  }

  returnHome(){
    this.gotoreturn.emit()
    this.route.navigate(['/bibliotheque']);

  }
  
  formatDate(datetime: string) {
    const date = datetime.slice(0, 10);
    var formattedDate = date.split('-')[0];
    return formattedDate;
  }

  getAll(){
    this.livreService.getAll()
    .subscribe(
      data =>{
        this.book = data.content;
        console.log(data.content);
      },
      error =>{
        this.error = error;
        console.log(error);
        
      }
    )
  }

}
