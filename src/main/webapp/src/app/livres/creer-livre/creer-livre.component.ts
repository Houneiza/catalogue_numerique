import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import{ FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-creer-livre',
  templateUrl: './creer-livre.component.html',
  styleUrls: ['./creer-livre.component.css']
})
export class CreerLivreComponent implements OnInit {

  livreForm: FormGroup;
  submitted = false;

  constructor(private route: Router,
              private fb: FormBuilder,) { }

  ngOnInit(): void {
    this.livreForm = this.fb.group({
      nomAuteur: ['', Validators.required],
      titre: ['', Validators.required],
      edition:['', Validators.required],
      Cat:['', Validators.required],
      anneeParution: ['', Validators.required],
      image:['', Validators.required],
    }); 
  }

}
