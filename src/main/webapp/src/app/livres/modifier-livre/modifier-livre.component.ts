import { Component, OnInit } from '@angular/core';
import{ FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { LivreInput } from 'src/app/models/livreInput';
import { LivreService } from 'src/app/services/livre.service';
import { first } from 'rxjs/operators';
import { CategorieService } from 'src/app/services/categorie.service';
import { AuteurService } from 'src/app/services/auteur.service';

declare const $: any;

@Component({
  selector: 'app-modifier-livre',
  templateUrl: './modifier-livre.component.html',
  styleUrls: ['./modifier-livre.component.css']
})
export class ModifierLivreComponent implements OnInit {

  livreForm: FormGroup;
  submitted = false;
  livreInput: LivreInput;
  error = '';
  livreId: number;
  titre: FormControl;
  image: FormControl;
  imageFile: string;
  anneeParution: FormControl;
  maisonEdition: FormControl;
  cote: FormControl;
  categorie: FormControl;
  auteur: FormControl;
  categories: [];
  auteurs: [];
  imageUrl: any;

  constructor(private route: Router,
              private fb: FormBuilder,
              private livreService: LivreService,
              private router: ActivatedRoute,
              private categorieService: CategorieService,
              private auteurService: AuteurService) { }

  ngOnInit(): void {
    this.livreId = parseInt(this.router.snapshot.paramMap.get('id'));
    console.log(this.livreId);
    this.titre = new FormControl('', Validators.required),
    this.image = new FormControl('', Validators.required),
    this.anneeParution = new FormControl('', Validators.required),
    this.maisonEdition = new FormControl('', Validators.required),
    this.cote = new FormControl('', Validators.required),
    this.categorie = new FormControl('', Validators.required),
    this.auteur = new FormControl('', Validators.required),
    this.livreForm = new FormGroup({
      titre: this.titre,
      image: this.image,
      anneeParution: this.anneeParution,
      maisonEdition: this.maisonEdition,
      cote: this.cote,
      categorie: this.categorie,
      auteur: this.auteur,
      //prenomAuteur: ['', Validators.required]
    });

    /* this.livreForm = this.fb.group({
      nomAuteur: ['', Validators.required],
      titre: ['', Validators.required],
      edition:['', Validators.required],
      sousCat:['', Validators.required],
      datePublication: ['', Validators.required],
      image:['', Validators.required],
    }); */

    this.livreService.findById(this.livreId)
    .subscribe(
      data =>{
        this.titre.setValue(data.body.titre);
        //this.image.setValue(data.body.image);
        this.imageUrl = data.body.image;
        this.anneeParution.setValue(data.body.anneeParution.slice(0, 10));
        this.maisonEdition.setValue(data.body.maisonEdition);
        this.cote.setValue(data.body.cote);
        this.categorie.setValue(data.body.categorie.name);
        this.auteur.setValue(data.body.auteur.nom);
      },
      error =>{
        this.error = error;
        //console.log(error);
        
      }
    )

    this.cat();
    this.aut();
    $('#zoneImage').change((e: Event) => this.onImageChange(e));

  }

  get flivre(){
    return this.livreForm.controls;
  }

  /* formatDate(datetime: string,separator: string) {
    const date = datetime.slice(0, 10);
    const dateTab = date.split('-');
    var formattedDate = dateTab[2]+separator+dateTab[1]+separator+dateTab[0];
    
    return formattedDate;
  } */

  formatDate(datetime: string) {
    const date = datetime.slice(0, 10);
    var formattedDate = date.split('-')[0];
    return formattedDate;
  }

  onImageChange(event) {

    if (event.target.files && event.target.files[0]) {
      let file =  event.target.files[0]
        console.log(file);
        let reader = new FileReader();
        reader.readAsDataURL(file)
        reader.onload = (e) => {
          this.imageUrl = e.target.result;
          this.imageFile = e.target.result.toString();
        }
    }
  }

  cat(){
    this.categorieService.getAll()
    .subscribe(
      data => {
        this.categories = data.content;
      }
    )
  }

  aut(){
    this.auteurService.getAll()
    .subscribe(
      data =>{
        this.auteurs = data.content;
      }
    )
  }

  update(){
    this.submitted = true;

    /* if(this.livreForm.invalid){
      return;
    } */
console.log("bonjour");

    this.livreInput = new LivreInput(
                        this.livreId,
                        this.titre.value,
                        this.imageFile,
                        this.anneeParution.value,
                        this.maisonEdition.value,
                        this.cote.value,
                        this.categorie.value,
                        this.auteur.value,
                      ),

    console.log(this.livreInput);

    this.livreService.update(this.livreInput)
    .subscribe(
      data =>{
        console.log(data);
        this.route.navigate(['livres/liste-livre']);
      },
      error =>{
        this.error = error;
        console.log(error);
      }
    )
  }

}
