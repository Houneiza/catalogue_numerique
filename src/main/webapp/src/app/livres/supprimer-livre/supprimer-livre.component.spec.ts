import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerLivreComponent } from './supprimer-livre.component';

describe('SupprimerLivreComponent', () => {
  let component: SupprimerLivreComponent;
  let fixture: ComponentFixture<SupprimerLivreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerLivreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerLivreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
