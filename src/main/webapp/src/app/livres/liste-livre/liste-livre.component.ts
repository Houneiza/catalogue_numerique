import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LivreInput } from 'src/app/models/livreInput';
import { LivreService } from 'src/app/services/livre.service';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { CategorieInput } from 'src/app/models/categorieInput';
import { AuteurInput } from 'src/app/models/auteurInput';
import { CategorieService } from 'src/app/services/categorie.service';
import { AuteurService } from 'src/app/services/auteur.service';

declare const $: any;

@Component({
  selector: 'app-liste-livre',
  templateUrl: './liste-livre.component.html',
  styleUrls: ['./liste-livre.component.css']
})
export class ListeLivreComponent implements OnInit {

  submitted = false;
  livreForm: FormGroup;
  search;
  page: Number = 1; 
  totalrecords: string;
  livreInput: LivreInput;
  error = '';
  book : [];
  livreId: number;
  titre: string;
  image: string;
  anneeParution: Date;
  maisonEdition: string;
  cote: string;
  categorie: CategorieInput;
  auteur: AuteurInput;
  categories: [];
  auteurs: [];

  imageUrl: any;

  application = [
    {
      nom: 'Alain',titre: 'Guide du traitement des déchets',edition: '5', sousCat: 'agep', datePublication: '2009',
    },
    {
      nom: 'fomo',titre: 'hello',edition: '7', sousCat: 'which', datePublication: '11-08-20',
    },
    {
      nom: 'albert', titre: 'l"homme musulman', edition: '7', sousCat: 'which', datePublication: '12-01-20', 
    },
    {
      nom: 'varshi',titre: 'mère',edition: '7', sousCat: 'which', datePublication: '11-01-18',
    },
    {
      nom: 'bibi',titre: 'la femme musulmane',edition: '7', sousCat: 'which', datePublication: '11-01-21',  
    },
    {
      nom: 'mum',titre: 'welches problem', edition: '7', sousCat: 'which', datePublication: '11-12-20',
    },
    {
      nom: 'indira',titre: 'beauty',edition: '7', sousCat: 'which', datePublication: '11-04-20', 
    },
    {
      nom: 'hanifa',titre: 'dame de fer',edition: '7', sousCat: 'which', datePublication: '11-01-20',
    }

  ]

  constructor( private fb: FormBuilder,
               private livreService: LivreService,
               private route: Router,
               private router: ActivatedRoute,
               private categorieService: CategorieService,
               private auteurService: AuteurService) { }

  ngOnInit(): void {
    this.livreForm = this.fb.group({
      titre: ['', Validators.required],
      image:['', Validators.required],
      anneeParution:['', Validators.required],
      categorie:['', Validators.required],
      maisonEdition:['', Validators.required],
      cote:['', Validators.required],
      auteur: ['', Validators.required]
    });

    this.getAll();
    this.cat();
    this.aut();
    $('#zoneImage').change((e: Event) => this.onImageChange(e))
  }

  formatDate(datetime: string) {
    const date = datetime.slice(0, 10);
    var formattedDate = date.split('-')[0];
    return formattedDate;
  }

  onImageChange(event) {

    if (event.target.files && event.target.files[0]) {
      let file =  event.target.files[0]
        console.log(file);
        let reader = new FileReader();
        reader.readAsDataURL(file)
        reader.onload = (e) => {
          this.imageUrl = e.target.result;
          this.image = e.target.result.toString();
        }
    }
  }
  

  get f(){
    return this.livreForm.controls;
  }

  create(){
    this.submitted = true;

    if(this.livreForm.invalid){
      console.log('formulaire invalide');
      console.log(this.livreForm);
      
      return;
    }

    var auteur = this.livreForm.controls['auteur'].value;
    var titre = this.livreForm.controls['titre'].value;
    var maisonEdition = this.livreForm.controls['maisonEdition'].value;
    var cote = this.livreForm.controls['cote'].value;
    var categorie = this.livreForm.controls['categorie'].value;
    var anneeParution = this.livreForm.controls['anneeParution'].value;

    this.livreInput = {
      id: null,
      auteur: auteur,
      titre: titre,
      maisonEdition: maisonEdition,
      cote: cote,
      image: this.image,
      categorie: categorie,
      anneeParution: anneeParution,
    };

    console.log(this.livreInput);
    
    this.livreService.create(this.livreInput)
    .subscribe(
      data =>{
        console.log(data);
        this.getAll();
      },
      error =>{
        this.error = error;
        console.log(this.error);
        
      }
    )
  }

  cat(){
    this.categorieService.getAll()
    .subscribe(
      data => {
        this.categories = data.content;
      }
    )
  }

  aut(){
    this.auteurService.getAll()
    .subscribe(
      data => {
        this.auteurs = data.content;
      }
    )
  }

  getAll(){
    this.livreService.getAll()
    .subscribe(
      data =>{
        this.book = data.content;
        console.log(data.content);
      },
      error =>{
        this.error = error;
        console.log(error);
        
      }
    )
  }

  findById(){
    this.livreId = parseInt(this.router.snapshot.paramMap.get('id'));
    console.log(this.livreId);
    this.livreService.findById(this.livreId)
    .pipe(first())
    .subscribe(
      data => {
        this.titre = data.content.titre;
        this.image = data.content.image;
        this.anneeParution = data.content.anneeParution;
        this.maisonEdition = data.content.maisonEdition;
        this.cote = data.content.cote;
        this.categorie = data.content.categorie;
        this.auteur = data.content.auteur;
        console.log(data.content);
      },
      error => {
        this.error = error;
        console.log(error);
        
      }
    )
  }

  delete(id){
    //this.livreId = parseInt(this.router.snapshot.paramMap.get('id'));
    //console.log(this.livreId);
    this.livreService.delete(id)
    .pipe(first())
    .subscribe(
      data => {
        console.log(id);
        this.getAll();
      },
      error => {
        this.error = error;
        console.log(error);
      }
    )
  }

}
