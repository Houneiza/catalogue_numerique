import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthentifService } from '../services/authentif.service';

declare const $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() connect = new EventEmitter();
  @Output() gotoHome = new EventEmitter();

  loginForm: FormGroup;
  submitted = false;
  error = '';
  currentMessage: String;
  thread: any;
  search;

  messages: string[] = [
    'L’ENSPM (Ex Institut Supérieur du Sahel) de l’Université de Maroua est un pôle d’Excellence polytechnique au service du développement local, national et sous-regional.',
    'Elle a pour missions spécifiques d’assurer : La formation et la recherche dans les domaines suivants : Agriculture, Elevage et Produits Dérivés , Energies Renouvelables , Sciences Environnementales, Génie Civil et Architecture, Informatique et Télécommunications, Génie Textile et Cuir, Hydraulique et Maîtrise des Eaux, Météorologie, Climatologie, Hydrologie et Pédologie.',
    'La bibliothèque de l’ENSPM est ouverte à tous les étudiants et dispose des livres de multitudes de domaines parmi lesquel: information, agriculture, élévage et produits, droit humanitaire, mathématiques, genie chimique, technologie textile et design, journaux et revue, etc...',
    'De ce fait, via ce catalogue, vous pouvez vous renseigner sur la présence d’un livre au sein de la bibliothèque ou que vous soyez',
  ];
  
  constructor(private route: Router,
              private authService: AuthentifService,
              private formBuilder: FormBuilder,) 
              { 
                /* if(this.authService.currentUserValue){
                  this.route.navigate(['/home'])
                } */
              }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({ // crée une instance de formGroup
      username: ['', Validators.compose([Validators.required,Validators.minLength(4)])],    // crée une instance de formControl
      password: ['', Validators.compose([Validators.required,Validators.minLength(8), Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+[a-zA-Z0-9-.]+$')])],     // crée une instance de formControl
    });
    
  }

  get flogin(){
    return this.loginForm.controls;
  }

  // méthode appelée lors du clic sur le bouton "login" 

  home(){
    this.gotoHome.emit();
    this.route.navigate(['/home']);
  }

  login() {

    this.submitted = true;

     /* if (this.loginForm.invalid) {
      return;
     } */

    this.connect.emit();
    console.log(this.loginForm);
    this.route.navigate(['/livres/liste-livre']);
    
    /* this.route.navigateByUrl('/');
      this.authService.login(this.flogin.username.value, this.flogin.password.value)
      .subscribe(
        data => {
        },
        error => {
            this.error = error;
        });
       */
  }

}
