import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Categorie } from '../models/categorie';
import { Observable } from 'rxjs';
import { CategorieInput } from '../models/categorieInput';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  host: string = environment.url;

  constructor(private http: HttpClient) { }

create(categorie:CategorieInput): Observable<CategorieInput>{
  return this.http.post<CategorieInput>(this.host + "/categories", categorie);
} 

getAll(): Observable<any>{
  return this.http.get<any>(this.host + "/categories");
}

findById(id: number): Observable<any>{
  return this.http.get<any>(this.host + "/categories/"+ id);
}

update(categorie: CategorieInput): Observable<CategorieInput>{
  return this.http.put<CategorieInput>(this.host + "/categories/" + categorie.id, categorie);
}

delete(id: number): Observable<any>{
  return this.http.delete<any>(this.host + "/categories/" + id);
}

}
