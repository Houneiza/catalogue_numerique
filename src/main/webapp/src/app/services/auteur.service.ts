import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Auteur } from '../models/auteur';
import { AuteurInput } from '../models/auteurInput';

/* const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type':'application/json',
    Authorization: 'my-auth-token'
  })
}; */

@Injectable({
  providedIn: 'root'
})
export class AuteurService {

  host: string = environment.url;

  constructor(private http: HttpClient) { }

   create(author:AuteurInput): Observable<AuteurInput>{
      return this.http.post<AuteurInput>(this.host + "/auteurs/", author);
  } 

  getAll(): Observable<any>{
    return this.http.get<any>(this.host + "/auteurs");
  }

  findById(id: number): Observable<any>{
    return this.http.get<any>(this.host + "/auteurs/"+ id);
  }

  update(author: AuteurInput): Observable<AuteurInput>{
    return this.http.put<AuteurInput>(this.host + "/auteurs?id="+ author.id, author);
  }

  delete(id: number): Observable<any>{
    return this.http.delete<any>(this.host + "/auteurs/" + id);
  }

}
