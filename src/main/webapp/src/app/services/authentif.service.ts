import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UtilisateurInput } from '../models/utilisateurInput';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'content-type': 'application/json',
    Authorization: 'my-auth-token'
  })
};


@Injectable({
  providedIn: 'root'
})
export class AuthentifService {

  private currentUserSubject: BehaviorSubject<UtilisateurInput>
  private currentUser: Observable<UtilisateurInput>

  host: string = environment.url;

  constructor(private http: HttpClient) { 
    this.currentUserSubject = new BehaviorSubject<UtilisateurInput>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
}

public get currentUserValue(): UtilisateurInput {
  return this.currentUserSubject.value;
}
  login(name: string, password: string) {

    return this.http.get<any>(this.host + "/utilisateurs/login")
      .pipe(map(user => {
        //  permet le stockage des détails de l'utilisateur et le jeton jwt dans le stockage local pour garder l'utilisateur
        //connecté entre les actualisateurs de la page  
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);

        return user;
      })); 
  }

  logout() {
    // surpprime l'objet utilisateur de actuel du stockage local
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
