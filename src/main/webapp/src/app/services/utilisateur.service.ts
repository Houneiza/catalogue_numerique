import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { UtilisateurInput } from '../models/utilisateurInput';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  host: string = environment.url;

  constructor(private http: HttpClient) { }

  create(utilisateur: UtilisateurInput):Observable<UtilisateurInput>{
    return this.http.post<UtilisateurInput>(this.host + "/utilisateurs", utilisateur);
  }

  getAll(): Observable<any>{
    return this.http.get<any>(this.host + "/utilisateurs");
  }

  delete(id: number){
    return this.http.delete<any>(this.host + "/utilisateurs/" + id);
  }

}
