import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Livre } from '../models/livre';
import { LivreInput } from '../models/livreInput';

@Injectable({
  providedIn: 'root'
})
export class LivreService {

  host: string = environment.url;

  constructor(private http: HttpClient) { }

create(livre:LivreInput): Observable<LivreInput>{
  return this.http.post<LivreInput>(this.host + "/livres", livre);
} 

getAll(): Observable<any>{
  return this.http.get<any>(this.host + "/livres");
}

findById(id: number): Observable<any>{
  return this.http.get<any>(this.host + "/livres/"+ id);
}

update(livre: LivreInput): Observable<LivreInput>{
  return this.http.put<LivreInput>(this.host + "/livres/" + livre.id, livre);
}

delete(id: number): Observable<any>{
  return this.http.delete<any>(this.host + "/livres/" + id);
}

}
