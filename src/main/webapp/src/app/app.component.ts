import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  constructor(private route:Router){}
  ngOnInit(): void {
    let session = localStorage.getItem('session')
    if(session != null){
      let sessionObject = JSON.parse(session)
      if(sessionObject.isUserConnected != null && sessionObject.isUserConnected){
        this.isConnect = true
        return;
      }
    }
   /*  alert(this.isConnect) */
  }

  isConnect: boolean = false;
  offConnect: number = 1;
  title = 'FrontendCatalogueNumerique';

  IsConnect(connect: number){
    this.offConnect = connect;
  }

  setIsConnect(connect: boolean){
    this.isConnect = connect;
    /* 
    this.route.navigateByUrl('/');
    console.log(connect)
    localStorage.setItem('session', JSON.stringify({
      isUserConnected: true
    })); */
  }

}
