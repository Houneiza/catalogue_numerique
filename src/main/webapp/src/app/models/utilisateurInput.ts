export class UtilisateurInput{
    constructor(private id: number,
                private nom: string,
                private password: string,
                private statut: boolean){}
}