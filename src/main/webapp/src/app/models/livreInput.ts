import { Auteur } from "./auteur";
import { Categorie } from "./categorie";
import { CategorieInput } from './categorieInput';
import { AuteurInput } from './auteurInput';

export class LivreInput{
    constructor(public id: number,
                public titre: string,
                public image: string,
                public anneeParution: Date,
                public maisonEdition: string,
                public cote: string,
                public categorie: CategorieInput,
                public auteur: AuteurInput){}
}