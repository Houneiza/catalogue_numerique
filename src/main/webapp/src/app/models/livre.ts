import { Auteur } from "./auteur";
import { Categorie } from "./categorie";

export class Livre{
    id: number;
    titre: string;
    image: string;
    anneeParution: Date;
    maisonEdition: string;
    cote: string;
    auteur: Auteur;
    categorie: Categorie;


}