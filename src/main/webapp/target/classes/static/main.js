(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _modules_general_home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/general/home/home.component */ "./src/app/modules/general/home/home.component.ts");
/* harmony import */ var _modules_general_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/general/not-found/not-found.component */ "./src/app/modules/general/not-found/not-found.component.ts");






const routes = [
    { path: '', component: _modules_general_home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"], },
    { path: 'contact',
        loadChildren: () => __webpack_require__.e(/*! import() | modules-general-contact-contact-module */ "modules-general-contact-contact-module").then(__webpack_require__.bind(null, /*! ./modules/general/contact/contact.module */ "./src/app/modules/general/contact/contact.module.ts"))
            .then(mod => mod.ContactModule) },
    { path: 'about',
        loadChildren: () => __webpack_require__.e(/*! import() | modules-general-about-about-module */ "modules-general-about-about-module").then(__webpack_require__.bind(null, /*! ./modules/general/about/about.module */ "./src/app/modules/general/about/about.module.ts"))
            .then(mod => mod.AboutModule) },
    { path: 'signin',
        loadChildren: () => __webpack_require__.e(/*! import() | modules-general-signin-signin-module */ "modules-general-signin-signin-module").then(__webpack_require__.bind(null, /*! ./modules/general/signin/signin.module */ "./src/app/modules/general/signin/signin.module.ts"))
            .then(mod => mod.SigninModule) },
    { path: '**', component: _modules_general_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_3__["NotFoundComponent"] },
    {
        path: 'components',
        loadChildren: () => __webpack_require__.e(/*! import() | modules-application-components-components-module */ "modules-application-components-components-module").then(__webpack_require__.bind(null, /*! ./modules/application/components/components.module */ "./src/app/modules/application/components/components.module.ts"))
            .then(mod => mod.ComponentsModule)
    },
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
                declarations: []
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



class AppComponent {
    constructor() {
        this.title = 'Catalogue';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 40, vars: 0, consts: [[1, "app"], [1, "navbar", "navbar-expand-md", "fixed-top", "navbar-dark", 2, "background-color", "#212121"], ["routerLink", "/", 1, "navbar-brand"], [1, "mr-1", 2, "color", "white"], ["src", "./assets/params/images/logo/ganatan.png", "width", "20", "height", "20", "alt", "ganatan"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarsExampleDefault", "aria-controls", "navbarsExampleDefault", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler"], [1, "navbar-toggler-icon"], ["id", "navbarsExampleDefault", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "ml-auto"], [1, "nav-item", "active"], ["routerLink", "/", 1, "nav-link"], [1, "fas", "fa-home", "mr-1"], ["routerLink", "/about", 1, "nav-link"], [1, "far", "fa-question-circle", "mr-1"], ["routerLink", "/contact", 1, "nav-link"], [1, "fas", "fa-envelope", "mr-1"], ["routerLink", "/signin", 1, "nav-link"], [1, "fas", "fa-user", "mr-1"], ["href", "https://github.com/ganatan", 1, "nav-link"], [1, "fab", "fa-github", "mr-1"], [1, "container-fluid"], [1, "footer"], [1, "container"], [1, "row"], [1, "col-12"], [1, "text-center", "text-white"]], template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "header");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "nav", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "ganatan");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "span", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "ul", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "li", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "i", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "home ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "li", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "i", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "About ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "li", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "i", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Contact ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "li", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "a", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "i", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Sign in ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "li", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "i", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Github ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "main");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "\u00A9 2020 - www.ganatan.com");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: [".app[_ngcontent-%COMP%]   main[_ngcontent-%COMP%], .app[_ngcontent-%COMP%]   footer[_ngcontent-%COMP%] {\n  padding-left: 1rem;\n}\n\n.footer[_ngcontent-%COMP%] {\n  background: #3F729B;\n  padding: 2em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7QUFDRjs7QUFFQTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtBQUNGIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFwcCBtYWluLCAuYXBwIGZvb3RlciB7XG4gIHBhZGRpbmctbGVmdDogMXJlbTtcbn1cblxuLmZvb3RlciB7XG4gIGJhY2tncm91bmQ6ICMzRjcyOUI7XG4gIHBhZGRpbmc6IDJlbTtcbn1cbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.scss']
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _modules_general_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/general/home/home.component */ "./src/app/modules/general/home/home.component.ts");
/* harmony import */ var _modules_general_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/general/not-found/not-found.component */ "./src/app/modules/general/not-found/not-found.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");







class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
        _modules_general_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"],
        _modules_general_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__["NotFoundComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                    _modules_general_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"],
                    _modules_general_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__["NotFoundComponent"],
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                ],
                providers: [],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/modules/general/home/home.component.ts":
/*!********************************************************!*\
  !*** ./src/app/modules/general/home/home.component.ts ***!
  \********************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class HomeComponent {
    ngOnInit() {
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 199, vars: 0, consts: [["id", "loader-wrapper"], ["id", "loader"], [1, "loader-section", "section-left"], [1, "loader-section", "section-right"], [1, "tm-main"], [1, "tm-welcome-section"], [1, "container", "tm-navbar-container"], [1, "row"], [1, "col-xl-12"], [1, "navbar", "navbar-expand-sm"], [1, "navbar-nav", "ml-auto"], [1, "nav-item", "active"], ["href", "index.html", 1, "nav-link", "tm-nav-link", "tm-text-white", "active"], [1, "nav-item"], ["href", "about.html", 1, "nav-link", "tm-nav-link", "tm-text-white"], ["href", "contact.html", 1, "nav-link", "tm-nav-link", "tm-text-white"], [1, "container", "text-center", "tm-welcome-container"], [1, "tm-welcome"], [1, "fas", "tm-fa-big", "fa-music", "tm-fa-mb-big"], [1, "text-uppercase", "mb-3", "tm-site-name"], [1, "tm-site-description"], [1, "container"], [1, "tm-search-form-container"], ["action", "index.html", "method", "GET", 1, "form-inline", "tm-search-form"], [1, "text-uppercase", "tm-new-release"], [1, "form-group", "tm-search-box"], ["type", "text", "name", "keyword", "placeholder", "Type your keyword ...", 1, "form-control", "tm-search-input"], ["type", "submit", "value", "Search", 1, "form-control", "tm-search-submit"], [1, "form-group", "tm-advanced-box"], ["href", "#", 1, "tm-text-white"], [1, "row", "tm-albums-container", "grid"], [1, "col-sm-6", "col-12", "col-md-6", "col-lg-3", "col-xl-3", "tm-album-col"], [1, "effect-sadie"], ["src", "img/insertion-260x390-01.jpg", "alt", "Image", 1, "img-fluid"], ["src", "img/insertion-260x390-02.jpg", "alt", "Image", 1, "img-fluid"], ["src", "img/insertion-260x390-03.jpg", "alt", "Image", 1, "img-fluid"], ["src", "img/insertion-260x390-04.jpg", "alt", "Image", 1, "img-fluid"], [1, "col-lg-12"], [1, "tm-tag-line"], [1, "tm-tag-line-title"], [1, "row", "mb-5"], [1, "media-boxes"], [1, "media"], ["src", "img/insertion-140x140-01.jpg", "alt", "Image", 1, "mr-3"], [1, "media-body", "tm-bg-gray"], [1, "tm-description-box"], [1, "tm-text-blue"], [1, "mb-0"], ["href", "https://plus.google.com/+tooplate", "target", "_parent"], [1, "tm-buy-box"], ["href", "#", 1, "tm-bg-blue", "tm-text-white", "tm-buy"], [1, "tm-text-blue", "tm-price-tag"], ["src", "img/insertion-140x140-02.jpg", "alt", "Image", 1, "mr-3"], [1, "media-body", "tm-bg-pink-light"], [1, "tm-text-pink"], ["href", "#", 1, "tm-bg-pink", "tm-text-white", "tm-buy"], [1, "tm-text-pink", "tm-price-tag"], ["src", "img/insertion-140x140-03.jpg", "alt", "Image", 1, "mr-3"], ["src", "img/insertion-140x140-04.jpg", "alt", "Image", 1, "mr-3"], ["src", "img/insertion-140x140-05.jpg", "alt", "Image", 1, "mr-3"], [1, "row", "tm-mb-big", "tm-subscribe-row"], [1, "col-xl-6", "col-lg-6", "col-md-12", "col-sm-12", "tm-bg-gray", "tm-subscribe-form"], [1, "tm-text-pink", "tm-mb-30"], [1, "tm-mb-30"], ["action", "index.html", "method", "POST"], [1, "form-group", "mb-0"], ["type", "text", "placeholder", "Your Email", 1, "form-control", "tm-subscribe-input"], ["type", "submit", "value", "Submit", 1, "tm-bg-pink", "tm-text-white", "d-block", "ml-auto", "tm-subscribe-btn"], [1, "col-xl-6", "col-lg-6", "col-md-12", "col-sm-12", "img-fluid", "pl-0", "tm-subscribe-img"], [1, "row", "tm-mb-medium"], [1, "col-xl-4", "col-lg-4", "col-md-4", "col-sm-6", "mb-4"], [1, "mb-4", "tm-font-300"], ["href", "#", 1, "tm-text-blue-dark", "d-block", "mb-2"], ["href", "#", 1, "tm-text-blue-dark", "d-block"], [1, "col-xl-4", "col-lg-4", "col-md-4", "col-sm-6"], [1, "text-center", "p-4"], [1, "tm-current-year"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "nav", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "ul", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "li", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "li", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "About");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "li", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Contact");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "i", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "h1", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Insertion");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "New HTML Website Template");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "form", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "New Release");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "input", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "input", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "a", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Go Advanced ...");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "figure", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "img", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "figcaption");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "First Album");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Rollover text and description text goes here over mouse over...");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "figure", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "img", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "figcaption");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "Album Two");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Maecenas iaculis et turpis et iaculis. Aenean at volutpat diam.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "figure", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "img", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "figcaption");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "Third Album");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "Vivamus eget elit purus. Nullam consectetur porttitor elementum.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "figure", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "img", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "figcaption");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "h2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68, "Album Four");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "Praesent nec feugiat dolor, elementum mollis purus. Etiam faucibus.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "h2", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Music is your powerful energy.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](80, "img", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "h5", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Vivamus eget urna vitae ante");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "p", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "Insertion HTML Template includes 3 different pages. You can use this layout for your website. Please tell your friends about ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "a", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, "Tooplate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, ". Thank you.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "a", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "buy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "span", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "$5.50");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](96, "img", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "h5", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](100, "Proin fermentum sapien sed nisl");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "p", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "Donec est felis, posuere viverra dapibus ac, pretium vel libero. Aliquam consectetur, arcu eget euismod congue, tortor metus vehicula.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "a", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](105, "buy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "span", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](107, "$7.25");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "img", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "h5", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "Quisque dignissim porta nunc");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "p", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, "In neque felis, fringilla vel orci ut, sodales consectetur purus. Vivamus eget urna vitae ante pellentesque iaculis. Praesent sit amet.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "a", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "buy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "span", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "$6.80");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](122, "img", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "h5", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "Vestibulum mattis quam sodales");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "p", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, "Curabitur id tempor orci. Fusce efficitur in enim sit amet sodales. Proin id gravida leo. Phasellus non quam et justo faucibus rhoncus.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](129, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "a", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](131, "buy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "span", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](133, "$3.75");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](135, "img", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "h5", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](139, "Vestibulum mattis quam sodales");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "p", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](141, "Maecenas sit amet nibh faucibus, tincidunt nisl sit amet, elementum eros. Fusce congue ligula gravida lorem lacinia.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "a", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](144, "buy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "span", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "$5.25");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "div", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "div", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "h3", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](150, "Subscribe our updates!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "p", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](152, "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi semper, ligula et pretium porttitor, leo orci accumsan ligula.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "form", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](155, "input", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](156, "input", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](157, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "div", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "div", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "h4", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](161, "Latest Albums");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](162, "a", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](163, "Sed fringilla consectetur");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](164, "a", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](165, "Mauris porta nisl quis");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "a", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](167, "Quisque maximus quam nec");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](168, "a", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](169, "Class aptent taciti sociosqu ad");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "div", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](171, "h4", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](172, "Our Pages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "a", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](174, "Nam dapibus imperdiet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "a", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](176, "Primis in faucibus orci");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](177, "a", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](178, "Sed interdum blandit dictum");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](179, "a", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](180, "Donec non blandit nisl");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "div", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "h4", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](183, "Quick Links");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "a", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](185, "Nullam scelerisque mauris");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](186, "a", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](187, "Vivamus tristique enim non orci");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "a", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](189, "Luctus et ultrices posuere");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](190, "a", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](191, "Cubilia Curae");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](192, "footer", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](193, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](194, "p", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](195, "Copyright \u00A9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](196, "span", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](197, "2018");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](198, " Your Company Name - Design: Tooplate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["body[_ngcontent-%COMP%] {\n  font-family: \"Open Sans\", Helvetica, Arial, sans-serif;\n  font-size: 17px;\n  font-weight: 300;\n  color: #666666;\n  overflow-x: hidden;\n}\n\na[_ngcontent-%COMP%] {\n  transition: all 0.3s ease;\n}\n\na[_ngcontent-%COMP%]:hover, a[_ngcontent-%COMP%]:focus, a.active[_ngcontent-%COMP%] {\n  color: #58d5ff;\n  text-decoration: none;\n}\n\n.tm-welcome-section[_ngcontent-%COMP%] {\n  height: 83vh;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  background-color: #E7A7C0;\n  background-image: url('insertion-header.jpg');\n  background-repeat: no-repeat;\n  background-size: cover;\n  background-position: center center;\n  color: white;\n  max-height: 700px;\n  min-height: 400px;\n}\n\n.tm-fa-big[_ngcontent-%COMP%] {\n  font-size: 5em;\n}\n\n.tm-fa-mb-big[_ngcontent-%COMP%] {\n  margin-bottom: 3rem;\n}\n\n.tm-navbar-container[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0;\n}\n\n.navbar-nav[_ngcontent-%COMP%] {\n  margin-right: 0;\n}\n\n.nav-link[_ngcontent-%COMP%] {\n  font-size: 1.4rem;\n}\n\n.tm-site-description[_ngcontent-%COMP%] {\n  font-size: 1.6rem;\n  color: white;\n}\n\n.navbar-expand-sm[_ngcontent-%COMP%]   .navbar-nav[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%] {\n  padding: 10px 20px;\n}\n\n.navbar-nav[_ngcontent-%COMP%] {\n  flex-direction: row;\n}\n\n.tm-site-name[_ngcontent-%COMP%] {\n  font-size: 4rem;\n}\n\n.tm-search-form-container[_ngcontent-%COMP%] {\n  border-radius: 15px;\n  border: 4px solid white;\n  background-color: #b35e6c;\n  margin-top: -54px;\n  box-shadow: 0px 0px 5px 0px rgba(0, 0, 0, 0.75);\n}\n\n.tm-search-form[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 96px;\n  font-size: 1.2rem;\n}\n\n.tm-search-box[_ngcontent-%COMP%] {\n  padding-left: 28px;\n  padding-right: 28px;\n  width: 60%;\n}\n\n.tm-advanced-box[_ngcontent-%COMP%] {\n  width: 20%;\n}\n\n.form-inline[_ngcontent-%COMP%]   .form-control.tm-search-input[_ngcontent-%COMP%] {\n  margin-right: 20px;\n  padding: 10px 15px;\n  background: transparent;\n  color: white;\n  width: 70%;\n  border-radius: 0;\n}\n\n.form-control.tm-search-input[_ngcontent-%COMP%]::placeholder {\n  \n  color: #fff;\n  opacity: 1;\n  \n}\n\n.form-control[_ngcontent-%COMP%]::placeholder {\n  \n  color: #666666;\n  opacity: 1;\n  \n}\n\n.form-control[_ngcontent-%COMP%]:-ms-input-placeholder {\n  \n  color: #666666;\n}\n\n.form-control[_ngcontent-%COMP%]::-ms-input-placeholder {\n  \n  color: #666666;\n}\n\n.form-control[_ngcontent-%COMP%] {\n  font-weight: 300;\n  border-radius: 0;\n  padding: 12px 20px;\n}\n\n.form-control[_ngcontent-%COMP%]:focus {\n  border-color: white;\n  box-shadow: none;\n}\n\n.tm-search-submit[_ngcontent-%COMP%] {\n  padding: 10px 35px;\n  background: transparent;\n  color: white;\n  border-radius: 0;\n}\n\n.tm-search-submit[_ngcontent-%COMP%] {\n  cursor: pointer;\n  transition: all 0.3s ease;\n}\n\n.tm-search-submit[_ngcontent-%COMP%]:hover {\n  background-color: white;\n  color: #B25E6D;\n}\n\n.tm-albums-container[_ngcontent-%COMP%] {\n  margin: 80px auto;\n  max-width: 260px;\n}\n\n.tm-album-col[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n}\n\n.tm-new-release[_ngcontent-%COMP%] {\n  background-image: url('gradient-blue.png');\n  background-repeat: no-repeat;\n  background-size: cover;\n  color: #1C689A;\n  height: 100%;\n  border-top-left-radius: 10px;\n  border-bottom-left-radius: 10px;\n  padding-left: 50px;\n  padding-right: 50px;\n  display: flex;\n  align-items: center;\n  font-weight: 400;\n  width: 20%;\n}\n\n\n\n\n\n\n\n.grid[_ngcontent-%COMP%]   figure[_ngcontent-%COMP%] {\n  position: relative;\n  float: left;\n  overflow: hidden;\n  cursor: pointer;\n}\n\n.grid[_ngcontent-%COMP%]   figure[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  position: relative;\n  display: block;\n  min-height: 100%;\n  max-width: 100%;\n}\n\n.grid[_ngcontent-%COMP%]   figure[_ngcontent-%COMP%]   figcaption[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  padding: 2em;\n  color: #fff;\n  -webkit-backface-visibility: hidden;\n          backface-visibility: hidden;\n}\n\nfigure.effect-sadie[_ngcontent-%COMP%]   figcaption[_ngcontent-%COMP%]::before {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: linear-gradient(to bottom, rgba(72, 76, 97, 0) 0%, rgba(72, 76, 97, 0.8) 75%);\n  content: \"\";\n  opacity: 0;\n  transform: translate3d(0, 50%, 0);\n}\n\nfigure.effect-sadie[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  font-size: 1.5rem;\n  padding: 1.2rem;\n  position: absolute;\n  bottom: 10%;\n  left: 0;\n  width: 100%;\n  opacity: 0;\n  transition: transform 0.35s, color 0.35s;\n  transform: translate3d(0, -80%, 0);\n}\n\nfigure.effect-sadie[_ngcontent-%COMP%]   figcaption[_ngcontent-%COMP%]::before, figure.effect-sadie[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  transition: opacity 0.35s, transform 0.35s;\n}\n\nfigure.effect-sadie[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  padding: 1.2rem;\n  margin-bottom: 0;\n  width: 100%;\n  opacity: 0;\n  transform: translate3d(0, 10px, 0);\n}\n\nfigure.effect-sadie[_ngcontent-%COMP%]:hover   h2[_ngcontent-%COMP%] {\n  opacity: 1;\n  transform: translate3d(0, -50%, 0) translate3d(0, -10px, 0);\n}\n\nfigure.effect-sadie[_ngcontent-%COMP%]:hover   figcaption[_ngcontent-%COMP%]::before, figure.effect-sadie[_ngcontent-%COMP%]:hover   p[_ngcontent-%COMP%] {\n  opacity: 1;\n  transform: translate3d(0, 0, 0);\n}\n\n.tm-tag-line[_ngcontent-%COMP%] {\n  margin-bottom: 60px;\n  position: relative;\n  min-height: 285px;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  background-image: url('insertion-1800x450.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n}\n\n.tm-tag-line-title[_ngcontent-%COMP%] {\n  font-size: 2rem;\n  color: white;\n  position: relative;\n  font-weight: 300;\n  margin-bottom: 0;\n  padding-left: 20px;\n  padding-right: 20px;\n  text-align: center;\n}\n\n.tm-font-300[_ngcontent-%COMP%] {\n  font-weight: 300;\n}\n\n.tm-text-gray[_ngcontent-%COMP%], .tm-link-gray[_ngcontent-%COMP%] {\n  color: #666666;\n}\n\n.tm-text-white[_ngcontent-%COMP%] {\n  color: #fff;\n}\n\n.tm-text-blue[_ngcontent-%COMP%] {\n  color: #6e9ece;\n}\n\n.tm-text-blue-dark[_ngcontent-%COMP%] {\n  color: #006699;\n}\n\n.tm-text-pink[_ngcontent-%COMP%] {\n  color: #D6688F;\n}\n\n.tm-text-pink-dark[_ngcontent-%COMP%] {\n  color: #CC6699;\n}\n\n.tm-text-brown[_ngcontent-%COMP%] {\n  color: #CC6666;\n}\n\n.tm-bg-gray[_ngcontent-%COMP%] {\n  background-color: #F2F2F2;\n}\n\n.tm-bg-pink[_ngcontent-%COMP%] {\n  background-color: #de7099;\n}\n\n.tm-bg-pink-light[_ngcontent-%COMP%] {\n  background-color: #f9e3eb;\n}\n\n.tm-bg-pink-light-2[_ngcontent-%COMP%] {\n  background-color: #EDAABC;\n}\n\n.tm-bg-blue[_ngcontent-%COMP%] {\n  background-color: #6699cc;\n}\n\n.media-boxes[_ngcontent-%COMP%] {\n  max-width: 930px;\n  margin: 0 auto;\n}\n\n.media[_ngcontent-%COMP%] {\n  margin-bottom: 35px;\n}\n\n.media-body[_ngcontent-%COMP%] {\n  display: flex;\n}\n\n.tm-buy-box[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: space-between;\n  width: 140px;\n}\n\n.tm-buy[_ngcontent-%COMP%] {\n  font-weight: 400;\n  width: 100%;\n  padding: 20px 45px;\n}\n\n.tm-bg-blue.tm-buy[_ngcontent-%COMP%]:hover, .tm-bg-blue.tm-buy[_ngcontent-%COMP%]:focus {\n  color: white;\n  background-color: #277bcc;\n}\n\n.tm-bg-pink.tm-buy[_ngcontent-%COMP%]:hover, .tm-bg-pink.tm-buy[_ngcontent-%COMP%]:focus {\n  background-color: #d14579;\n  color: white;\n}\n\n.tm-description-box[_ngcontent-%COMP%] {\n  padding: 30px 35px;\n}\n\n.tm-price-tag[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-size: 1.2rem;\n  font-weight: 400;\n}\n\n.tm-subscribe-form[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  padding: 50px;\n}\n\n.tm-subscribe-input[_ngcontent-%COMP%] {\n  width: 350px;\n  margin-bottom: 25px;\n  border-radius: 0;\n  border-color: #D6688F;\n  padding: 13px 15px;\n}\n\n.form-control.tm-subscribe-input[_ngcontent-%COMP%]:focus {\n  border-color: #df6f98;\n}\n\n.tm-subscribe-btn[_ngcontent-%COMP%] {\n  border: none;\n  padding: 12px 40px;\n  font-size: 1.2rem;\n  font-weight: 300;\n  cursor: pointer;\n  transition: all 0.3s ease;\n}\n\n.tm-subscribe-btn[_ngcontent-%COMP%]:hover, .tm-subscribe-focus[_ngcontent-%COMP%] {\n  background-color: #d14579;\n}\n\n.tm-subscribe-img[_ngcontent-%COMP%] {\n  background-image: url('insertion-600x400.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n  min-height: 400px;\n}\n\n.tm-about-img[_ngcontent-%COMP%] {\n  background-image: url('insertion-560x336-01.jpg');\n  background-size: cover;\n  background-repeat: no-repeat;\n  height: 336px;\n  max-width: 560px;\n}\n\n.tm-mb-30[_ngcontent-%COMP%] {\n  margin-bottom: 30px;\n}\n\n.tm-mb-medium[_ngcontent-%COMP%] {\n  margin-bottom: 90px;\n}\n\n.tm-mb-big[_ngcontent-%COMP%] {\n  margin-bottom: 130px;\n}\n\n.tm-mt-big[_ngcontent-%COMP%] {\n  margin-top: 100px;\n}\n\n.tm-about-h2[_ngcontent-%COMP%] {\n  font-size: 1.4rem;\n  font-weight: 300;\n}\n\n.tm-v-center[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n}\n\n.tm-media-v-center[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n  padding: 33px 30px;\n  cursor: pointer;\n  transition: all 0.3s ease;\n  width: 100%;\n  height: 100px;\n}\n\n.nav-tabs[_ngcontent-%COMP%]   .nav-item[_ngcontent-%COMP%] {\n  margin-bottom: 20px;\n  width: 100%;\n}\n\n.nav-tabs[_ngcontent-%COMP%]   .nav-item[_ngcontent-%COMP%]:last-child {\n  margin-bottom: 0;\n}\n\n.tm-media-link[_ngcontent-%COMP%] {\n  font-size: 1.2rem;\n}\n\n.tm-about-row[_ngcontent-%COMP%] {\n  max-width: 1190px;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.tm-about-text[_ngcontent-%COMP%] {\n  max-width: 560px;\n  padding: 0 50px;\n}\n\n.tm-about-description[_ngcontent-%COMP%] {\n  line-height: 1.8;\n}\n\n.tm-media-2[_ngcontent-%COMP%] {\n  padding: 55px;\n  margin-bottom: 0;\n  height: 100%;\n  align-items: center;\n}\n\n.tm-media-body-2[_ngcontent-%COMP%] {\n  flex-direction: column;\n}\n\n.tm-media-body-2[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n  align-self: flex-end;\n}\n\n.tm-media-2-icon[_ngcontent-%COMP%] {\n  margin-right: 60px;\n  align-self: flex-start;\n}\n\n.tm-media-2-header[_ngcontent-%COMP%] {\n  font-size: 1.4rem;\n  font-weight: 300;\n}\n\n.btn-secondary[_ngcontent-%COMP%] {\n  background-color: #33CCFF;\n  border-color: #33CCFF;\n  font-size: 1.1rem;\n  padding: 12px 40px;\n}\n\n.btn-secondary[_ngcontent-%COMP%]:focus, .btn-secondary[_ngcontent-%COMP%]:hover {\n  background-color: #269dc4;\n  border-color: #269dc4;\n}\n\n.nav-tabs[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%] {\n  border-radius: 0;\n  border: none;\n  color: #666666;\n}\n\n.nav-tabs[_ngcontent-%COMP%] {\n  border-bottom: 0;\n}\n\n.nav-tabs[_ngcontent-%COMP%]   .nav-item.show[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%], .nav-tabs[_ngcontent-%COMP%]   .nav-link.active[_ngcontent-%COMP%], .nav-tabs[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%]:hover {\n  background-color: #EDAABC;\n  color: white;\n}\n\n.tm-tab-links-container[_ngcontent-%COMP%], .tm-tab-content-container[_ngcontent-%COMP%] {\n  position: relative;\n  width: 100%;\n  min-height: 1px;\n}\n\n.tm-tab-links-container[_ngcontent-%COMP%] {\n  flex: 0 0 28%;\n  padding-right: 10px;\n  padding-left: 15px;\n}\n\n.tm-tab-content-container[_ngcontent-%COMP%] {\n  flex: 0 0 72%;\n  padding-right: 15px;\n  padding-left: 10px;\n}\n\n.tab-content[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n}\n\n.tm-contact-col[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n}\n\n.tm-contact-left[_ngcontent-%COMP%] {\n  flex: 0 0 20%;\n  margin-right: 20px;\n}\n\n.tm-contact-middle[_ngcontent-%COMP%] {\n  margin-right: 20px;\n  padding: 45px 50px;\n}\n\n.tm-contact-right[_ngcontent-%COMP%] {\n  padding: 45px 50px;\n}\n\n.tm-contact-middle[_ngcontent-%COMP%], .tm-contact-right[_ngcontent-%COMP%] {\n  flex: 0 0 38%;\n  max-width: 440px;\n}\n\n.tm-select[_ngcontent-%COMP%] {\n  border-radius: 0;\n  width: 100%;\n  padding: 12px 20px;\n}\n\n.tm-select-group[_ngcontent-%COMP%] {\n  padding: 8px 0;\n  display: flex;\n  justify-content: space-between;\n}\n\n.tm-radio-label[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n\n.tm-radio-label[_ngcontent-%COMP%]:last-child {\n  margin-right: 15px;\n}\n\niframe[_ngcontent-%COMP%] {\n  max-width: 100%;\n}\n\n#google-map[_ngcontent-%COMP%] {\n  border: 1px solid #fff;\n}\n\n\n\n#loader-wrapper[_ngcontent-%COMP%] {\n  position: fixed;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  z-index: 2000;\n}\n\n#loader[_ngcontent-%COMP%] {\n  display: block;\n  position: relative;\n  left: 50%;\n  top: 50%;\n  width: 150px;\n  height: 150px;\n  margin: -75px 0 0 -75px;\n  border-radius: 50%;\n  border: 3px solid transparent;\n  border-top-color: #3498db;\n  \n  animation: spin 2s linear infinite;\n  \n  z-index: 1001;\n}\n\n#loader[_ngcontent-%COMP%]:before {\n  content: \"\";\n  position: absolute;\n  top: 5px;\n  left: 5px;\n  right: 5px;\n  bottom: 5px;\n  border-radius: 50%;\n  border: 3px solid transparent;\n  border-top-color: #e74c3c;\n  \n  animation: spin 3s linear infinite;\n  \n}\n\n#loader[_ngcontent-%COMP%]:after {\n  content: \"\";\n  position: absolute;\n  top: 15px;\n  left: 15px;\n  right: 15px;\n  bottom: 15px;\n  border-radius: 50%;\n  border: 3px solid transparent;\n  border-top-color: #f9c922;\n  \n  animation: spin 1.5s linear infinite;\n  \n}\n\n@keyframes spin {\n  0% {\n    \n    \n    transform: rotate(0deg);\n    \n  }\n  100% {\n    \n    \n    transform: rotate(360deg);\n    \n  }\n}\n\n#loader-wrapper[_ngcontent-%COMP%]   .loader-section[_ngcontent-%COMP%] {\n  position: fixed;\n  top: 0;\n  width: 51%;\n  height: 100%;\n  background: #222222;\n  z-index: 1000;\n  \n  \n  transform: translateX(0);\n  \n}\n\n#loader-wrapper[_ngcontent-%COMP%]   .loader-section.section-left[_ngcontent-%COMP%] {\n  left: 0;\n}\n\n#loader-wrapper[_ngcontent-%COMP%]   .loader-section.section-right[_ngcontent-%COMP%] {\n  right: 0;\n}\n\n\n\n.loaded[_ngcontent-%COMP%]   #loader-wrapper[_ngcontent-%COMP%]   .loader-section.section-left[_ngcontent-%COMP%] {\n  \n  \n  transform: translateX(-100%);\n  \n  transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);\n}\n\n.loaded[_ngcontent-%COMP%]   #loader-wrapper[_ngcontent-%COMP%]   .loader-section.section-right[_ngcontent-%COMP%] {\n  \n  \n  transform: translateX(100%);\n  \n  transition: all 0.7s 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);\n}\n\n.loaded[_ngcontent-%COMP%]   #loader[_ngcontent-%COMP%] {\n  opacity: 0;\n  transition: all 0.3s ease-out;\n}\n\n.loaded[_ngcontent-%COMP%]   #loader-wrapper[_ngcontent-%COMP%] {\n  visibility: hidden;\n  \n  \n  transform: translateY(-100%);\n  \n  transition: all 0.3s 1s ease-out;\n}\n\n\n\n.no-js[_ngcontent-%COMP%]   #loader-wrapper[_ngcontent-%COMP%] {\n  display: none;\n}\n\n@media (min-width: 576px) {\n  .tm-albums-container[_ngcontent-%COMP%] {\n    max-width: none;\n  }\n\n  .navbar-expand-sm[_ngcontent-%COMP%]   .navbar-nav[_ngcontent-%COMP%]   .nav-link[_ngcontent-%COMP%] {\n    padding: 20px 40px;\n  }\n\n  .navbar-nav[_ngcontent-%COMP%] {\n    margin-right: -40px;\n  }\n}\n\n@media (min-width: 768px) {\n  .tm-albums-container[_ngcontent-%COMP%] {\n    max-width: 540px;\n  }\n}\n\n@media (min-width: 992px) {\n  .tm-albums-container[_ngcontent-%COMP%] {\n    max-width: none;\n  }\n\n  figure.effect-sadie[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n    bottom: 20%;\n  }\n}\n\n@media (min-width: 1200px) {\n  .container[_ngcontent-%COMP%] {\n    max-width: 1230px;\n  }\n\n  figure.effect-sadie[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n    bottom: 10%;\n  }\n\n  .tm-radio-label[_ngcontent-%COMP%]:last-child {\n    margin-right: 30px;\n  }\n}\n\n@media (max-width: 1199px) {\n  .tm-new-release[_ngcontent-%COMP%] {\n    font-size: 1.1rem;\n    padding-left: 35px;\n    padding-right: 35px;\n  }\n}\n\n@media (max-width: 991px) {\n  .tm-welcome-section[_ngcontent-%COMP%] {\n    height: 70vh;\n  }\n\n  .tm-about-col-left[_ngcontent-%COMP%] {\n    margin-bottom: 20px;\n  }\n\n  .tm-about-text[_ngcontent-%COMP%] {\n    padding: 50px;\n  }\n\n  .tm-about-text[_ngcontent-%COMP%], .tm-about-img[_ngcontent-%COMP%] {\n    margin-left: auto;\n    margin-right: auto;\n  }\n\n  .form-inline[_ngcontent-%COMP%]   .form-control.tm-search-input[_ngcontent-%COMP%] {\n    width: 60%;\n  }\n\n  .tm-advanced-box[_ngcontent-%COMP%] {\n    font-size: 1rem;\n    padding-right: 30px;\n  }\n\n  .tm-subscribe-img[_ngcontent-%COMP%] {\n    padding-right: 0;\n  }\n\n  .tm-mt-big[_ngcontent-%COMP%] {\n    margin-top: 60px;\n  }\n\n  .tm-mb-big[_ngcontent-%COMP%] {\n    margin-bottom: 80px;\n  }\n\n  .tm-mb-medium[_ngcontent-%COMP%] {\n    margin-bottom: 40px;\n  }\n\n  .tm-subscribe-row[_ngcontent-%COMP%] {\n    max-width: 600px;\n    margin-left: auto;\n    margin-right: auto;\n  }\n\n  .tm-tab-link[_ngcontent-%COMP%] {\n    padding: 15px 25px;\n  }\n\n  .tm-tab-link[_ngcontent-%COMP%]   .fa-2x[_ngcontent-%COMP%] {\n    font-size: 1.8rem;\n  }\n\n  .tm-tab-link[_ngcontent-%COMP%]   .tm-media-link[_ngcontent-%COMP%] {\n    font-size: 1rem;\n  }\n\n  .tm-tab-links-container[_ngcontent-%COMP%] {\n    flex: 0 0 100%;\n    padding-right: 15px;\n  }\n\n  .tm-tab-content-container[_ngcontent-%COMP%] {\n    flex: 0 0 100%;\n    margin-top: 15px;\n    padding-left: 15px;\n  }\n\n  .tm-media-2-icon[_ngcontent-%COMP%] {\n    margin-right: 30px;\n  }\n\n  .tm-media-2[_ngcontent-%COMP%] {\n    padding: 35px 30px;\n  }\n\n  .tm-contact-col[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n  }\n\n  .tm-contact-left[_ngcontent-%COMP%] {\n    flex: 0 0 30%;\n    margin-right: 0;\n  }\n\n  .tm-contact-middle[_ngcontent-%COMP%] {\n    flex: 0 0 67%;\n    margin-right: 0;\n    max-width: none;\n  }\n\n  .tm-contact-right[_ngcontent-%COMP%] {\n    flex: 0 0 100%;\n    margin-top: 20px;\n    max-width: none;\n  }\n\n  iframe[_ngcontent-%COMP%] {\n    width: 100%;\n  }\n}\n\n@media (max-width: 767px) {\n  .tm-welcome-section[_ngcontent-%COMP%] {\n    height: 60vh;\n  }\n\n  .tm-fa-big[_ngcontent-%COMP%] {\n    font-size: 4em;\n  }\n\n  .tm-fa-mb-big[_ngcontent-%COMP%] {\n    margin-bottom: 2rem;\n  }\n\n  .tm-site-name[_ngcontent-%COMP%] {\n    font-size: 3rem;\n  }\n\n  .tm-site-description[_ngcontent-%COMP%] {\n    font-size: 1.3rem;\n  }\n\n  .tm-new-release[_ngcontent-%COMP%] {\n    width: 100%;\n    padding: 20px;\n    justify-content: center;\n    border-top-right-radius: 10px;\n    border-bottom-left-radius: 0;\n  }\n\n  .tm-search-form[_ngcontent-%COMP%] {\n    flex-flow: column;\n    height: auto;\n  }\n\n  .form-inline[_ngcontent-%COMP%]   .form-group.tm-search-box[_ngcontent-%COMP%] {\n    margin-top: 20px;\n    margin-bottom: 20px;\n    width: 100%;\n  }\n\n  .form-inline[_ngcontent-%COMP%]   .form-control.tm-search-input[_ngcontent-%COMP%] {\n    width: calc(100% - 141px);\n  }\n\n  .form-inline[_ngcontent-%COMP%]   .form-control.tm-search-submit[_ngcontent-%COMP%] {\n    width: 121px;\n  }\n\n  .form-inline[_ngcontent-%COMP%]   .form-group.tm-advanced-box[_ngcontent-%COMP%] {\n    width: auto;\n    margin-bottom: 20px;\n    margin-top: 10px;\n    padding-right: 0;\n  }\n\n  .tm-search-form-container[_ngcontent-%COMP%] {\n    margin-top: -70px;\n  }\n\n  .tm-tab-link[_ngcontent-%COMP%] {\n    padding: 33px 30px;\n  }\n\n  .tm-tab-link[_ngcontent-%COMP%]   .fa-2x[_ngcontent-%COMP%] {\n    font-size: 2rem;\n  }\n\n  .tm-tab-link[_ngcontent-%COMP%]   .tm-media-link[_ngcontent-%COMP%] {\n    font-size: 1.4rem;\n  }\n}\n\n@media (max-width: 575px) {\n  .container[_ngcontent-%COMP%] {\n    max-width: 540px;\n  }\n\n  .form-inline[_ngcontent-%COMP%]   .form-control.tm-search-input[_ngcontent-%COMP%] {\n    width: 100%;\n    margin-bottom: 20px;\n  }\n\n  .tm-search-submit[_ngcontent-%COMP%] {\n    margin-left: auto;\n    margin-right: auto;\n  }\n\n  .tm-albums-container[_ngcontent-%COMP%] {\n    margin-top: 40px;\n    margin-bottom: 40px;\n  }\n\n  .tm-contact-left[_ngcontent-%COMP%], .tm-contact-middle[_ngcontent-%COMP%] {\n    flex: 0 0 100%;\n  }\n\n  .tm-contact-middle[_ngcontent-%COMP%] {\n    margin-top: 20px;\n  }\n\n  .tm-contact-middle[_ngcontent-%COMP%], .tm-contact-right[_ngcontent-%COMP%] {\n    padding: 35px 30px;\n  }\n}\n\n@media (max-width: 500px) {\n  .media[_ngcontent-%COMP%] {\n    flex-direction: column;\n  }\n\n  .media-body[_ngcontent-%COMP%] {\n    margin-top: 15px;\n  }\n}\n\n@media (max-width: 420px) {\n  .tm-subscribe-input[_ngcontent-%COMP%] {\n    width: 260px;\n  }\n\n  .tm-fa-big[_ngcontent-%COMP%] {\n    font-size: 3rem;\n  }\n\n  .tm-fa-mb-big[_ngcontent-%COMP%] {\n    margin-bottom: 1.5rem;\n  }\n\n  .tm-site-name[_ngcontent-%COMP%] {\n    font-size: 2.4rem;\n  }\n\n  .tm-site-description[_ngcontent-%COMP%] {\n    font-size: 1.2rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW9kdWxlcy9nZW5lcmFsL2hvbWUvaG9tZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNDLHNEQUFBO0VBQ0EsZUFBQTtFQUNHLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FBREo7O0FBSUE7RUFBSSx5QkFBQTtBQUFKOztBQUNBO0VBQ0ksY0FBQTtFQUNBLHFCQUFBO0FBRUo7O0FBQ0E7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLHlCQUFBO0VBQ0EsNkNBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0NBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQUVKOztBQUNBO0VBQWEsY0FBQTtBQUdiOztBQUZBO0VBQWdCLG1CQUFBO0FBTWhCOztBQUpBO0VBQ0ksa0JBQUE7RUFDQSxNQUFBO0FBT0o7O0FBSkE7RUFBYyxlQUFBO0FBUWQ7O0FBUEE7RUFBWSxpQkFBQTtBQVdaOztBQVRBO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0FBWUo7O0FBVEE7RUFBMEMsa0JBQUE7QUFhMUM7O0FBWkE7RUFBYyxtQkFBQTtBQWdCZDs7QUFmQTtFQUFnQixlQUFBO0FBbUJoQjs7QUFqQkE7RUFDSSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtFQUdBLCtDQUFBO0FBb0JKOztBQWpCQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUFvQko7O0FBakJBO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7QUFvQko7O0FBakJBO0VBQW1CLFVBQUE7QUFxQm5COztBQW5CQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUFzQko7O0FBbkJBO0VBQTZDLHlDQUFBO0VBQ3pDLFdBQUE7RUFDQSxVQUFBO0VBQVksWUFBQTtBQXdCaEI7O0FBYkE7RUFBNkIseUNBQUE7RUFDekIsY0FBQTtFQUNBLFVBQUE7RUFBWSxZQUFBO0FBNEJoQjs7QUF6QkE7RUFBc0MsNEJBQUE7RUFDbEMsY0FBQTtBQTZCSjs7QUExQkE7RUFBdUMsbUJBQUE7RUFDbkMsY0FBQTtBQThCSjs7QUEzQkE7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUE4Qko7O0FBM0JBO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtBQThCSjs7QUEzQkE7RUFDSSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FBOEJKOztBQTNCQTtFQUNJLGVBQUE7RUFDQSx5QkFBQTtBQThCSjs7QUEzQkE7RUFDSSx1QkFBQTtFQUNBLGNBQUE7QUE4Qko7O0FBM0JBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtBQThCSjs7QUEzQkE7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7QUE4Qko7O0FBM0JBO0VBQ0ksMENBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSw0QkFBQTtFQUNBLCtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtBQThCSjs7QUEzQkEsa0JBQUE7O0FBQ0Esa0JBQUE7O0FBQ0Esa0JBQUE7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUE2Qko7O0FBMUJBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBNkJKOztBQTFCQTtFQUNJLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsbUNBQUE7VUFBQSwyQkFBQTtBQTZCSjs7QUExQkE7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFFQSx5RkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBRUEsaUNBQUE7QUE2Qko7O0FBMUJBO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBRUEsd0NBQUE7RUFFQSxrQ0FBQTtBQTZCSjs7QUExQkE7O0VBR0ksMENBQUE7QUE2Qko7O0FBMUJBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBRUEsa0NBQUE7QUE2Qko7O0FBMUJBO0VBQ0ksVUFBQTtFQUVBLDJEQUFBO0FBNkJKOztBQTFCQTs7RUFFSSxVQUFBO0VBRUEsK0JBQUE7QUE2Qko7O0FBMUJBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSwrQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUE2Qko7O0FBMUJBO0VBQ0ksZUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUE2Qko7O0FBMUJBO0VBQWUsZ0JBQUE7QUE4QmY7O0FBN0JBO0VBQStCLGNBQUE7QUFpQy9COztBQWhDQTtFQUFpQixXQUFBO0FBb0NqQjs7QUFuQ0E7RUFBZ0IsY0FBQTtBQXVDaEI7O0FBdENBO0VBQXFCLGNBQUE7QUEwQ3JCOztBQXpDQTtFQUFnQixjQUFBO0FBNkNoQjs7QUE1Q0E7RUFBcUIsY0FBQTtBQWdEckI7O0FBL0NBO0VBQWlCLGNBQUE7QUFtRGpCOztBQWxEQTtFQUFjLHlCQUFBO0FBc0RkOztBQXJEQTtFQUFjLHlCQUFBO0FBeURkOztBQXhEQTtFQUFvQix5QkFBQTtBQTREcEI7O0FBM0RBO0VBQXNCLHlCQUFBO0FBK0R0Qjs7QUE5REE7RUFBYyx5QkFBQTtBQWtFZDs7QUFoRUE7RUFDSSxnQkFBQTtFQUNBLGNBQUE7QUFtRUo7O0FBaEVBO0VBQVMsbUJBQUE7QUFvRVQ7O0FBbkVBO0VBQWMsYUFBQTtBQXVFZDs7QUFyRUE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtBQXdFSjs7QUFyRUE7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQXdFSjs7QUFyRUE7O0VBRUksWUFBQTtFQUNBLHlCQUFBO0FBd0VKOztBQXJFQTs7RUFFSSx5QkFBQTtFQUNBLFlBQUE7QUF3RUo7O0FBckVBO0VBQXNCLGtCQUFBO0FBeUV0Qjs7QUF2RUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQTBFSjs7QUF2RUE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsYUFBQTtBQTBFSjs7QUF2RUE7RUFDSSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7QUEwRUo7O0FBdkVBO0VBQXlDLHFCQUFBO0FBMkV6Qzs7QUF6RUE7RUFDSSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0FBNEVKOztBQXpFQTs7RUFFSSx5QkFBQTtBQTRFSjs7QUF6RUE7RUFDSSw4Q0FBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxpQkFBQTtBQTRFSjs7QUF6RUE7RUFDSSxpREFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUE0RUo7O0FBekVBO0VBQVksbUJBQUE7QUE2RVo7O0FBNUVBO0VBQWdCLG1CQUFBO0FBZ0ZoQjs7QUEvRUE7RUFBYSxvQkFBQTtBQW1GYjs7QUFsRkE7RUFBYSxpQkFBQTtBQXNGYjs7QUFwRkE7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0FBdUZKOztBQXBGQTtFQUNJLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0FBdUZKOztBQXBGQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7QUF1Rko7O0FBcEZBO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0FBdUZKOztBQXBGQTtFQUFpQyxnQkFBQTtBQXdGakM7O0FBdkZBO0VBQWlCLGlCQUFBO0FBMkZqQjs7QUF6RkE7RUFDSSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUE0Rko7O0FBekZBO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0FBNEZKOztBQXpGQTtFQUF3QixnQkFBQTtBQTZGeEI7O0FBM0ZBO0VBQ0ksYUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBOEZKOztBQTNGQTtFQUFtQixzQkFBQTtBQStGbkI7O0FBOUZBO0VBQXdCLG9CQUFBO0FBa0d4Qjs7QUFoR0E7RUFDSSxrQkFBQTtFQUNBLHNCQUFBO0FBbUdKOztBQWhHQTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7QUFtR0o7O0FBaEdBO0VBQ0kseUJBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFtR0o7O0FBaEdBOztFQUVJLHlCQUFBO0VBQ0EscUJBQUE7QUFtR0o7O0FBaEdBO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQW1HSjs7QUFoR0E7RUFBWSxnQkFBQTtBQW9HWjs7QUFsR0E7OztFQUdJLHlCQUFBO0VBQ0EsWUFBQTtBQXFHSjs7QUFsR0E7O0VBRUksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQXFHSjs7QUFsR0E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQXFHSjs7QUFsR0E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQXFHSjs7QUFsR0E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUFxR0o7O0FBbEdBO0VBQ0ksYUFBQTtFQUNBLDhCQUFBO0FBcUdKOztBQWxHQTtFQUNJLGFBQUE7RUFDQSxrQkFBQTtBQXFHSjs7QUFsR0E7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0FBcUdKOztBQWxHQTtFQUFvQixrQkFBQTtBQXNHcEI7O0FBcEdBOztFQUVJLGFBQUE7RUFDQSxnQkFBQTtBQXVHSjs7QUFwR0E7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQXVHSjs7QUFwR0E7RUFDSSxjQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0FBdUdKOztBQXBHQTtFQUFrQixlQUFBO0FBd0dsQjs7QUF2R0E7RUFBNkIsa0JBQUE7QUEyRzdCOztBQTFHQTtFQUFTLGVBQUE7QUE4R1Q7O0FBN0dBO0VBQWMsc0JBQUE7QUFpSGQ7O0FBL0dBLFdBQUE7O0FBQ0E7RUFDSSxlQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUFrSEo7O0FBaEhBO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLHlCQUFBO0VBRTRDLGlDQUFBO0VBQzVDLGtDQUFBO0VBQW9DLHVDQUFBO0VBRXBDLGFBQUE7QUFtSEo7O0FBaEhBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSx5QkFBQTtFQUU0QyxpQ0FBQTtFQUM1QyxrQ0FBQTtFQUFvQyx1Q0FBQTtBQW9IeEM7O0FBakhBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7RUFDQSx5QkFBQTtFQUU4QyxpQ0FBQTtFQUM1QyxvQ0FBQTtFQUFzQyx1Q0FBQTtBQXFINUM7O0FBdEdBO0VBQ0k7SUFDc0MsbUNBQUE7SUFDSixTQUFBO0lBQzlCLHVCQUFBO0lBQTBCLCtCQUFBO0VBOEhoQztFQTVIRTtJQUN3QyxtQ0FBQTtJQUNKLFNBQUE7SUFDaEMseUJBQUE7SUFBNEIsK0JBQUE7RUFpSWxDO0FBQ0Y7O0FBOUhBO0VBQ0ksZUFBQTtFQUNBLE1BQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNtQyxtQ0FBQTtFQUNKLFNBQUE7RUFDL0Isd0JBQUE7RUFBMkIsK0JBQUE7QUFtSS9COztBQWhJQTtFQUNJLE9BQUE7QUFtSUo7O0FBaElBO0VBQ0ksUUFBQTtBQW1JSjs7QUFoSUEsV0FBQTs7QUFDQTtFQUMyQyxtQ0FBQTtFQUNBLFNBQUE7RUFDL0IsNEJBQUE7RUFBK0IsK0JBQUE7RUFHL0IsOERBQUE7QUFxSVo7O0FBbElBO0VBQzBDLG1DQUFBO0VBQ0EsU0FBQTtFQUM5QiwyQkFBQTtFQUE4QiwrQkFBQTtFQUd0Qyw4REFBQTtBQXVJSjs7QUFwSUE7RUFDSSxVQUFBO0VBRVEsNkJBQUE7QUF1SVo7O0FBcklBO0VBQ0ksa0JBQUE7RUFFdUMsbUNBQUE7RUFDQSxTQUFBO0VBQy9CLDRCQUFBO0VBQStCLCtCQUFBO0VBRy9CLGdDQUFBO0FBeUlaOztBQXRJQSwwQkFBQTs7QUFDQTtFQUF5QixhQUFBO0FBMEl6Qjs7QUF4SUE7RUFDSTtJQUF1QixlQUFBO0VBNEl6Qjs7RUEzSUU7SUFBMEMsa0JBQUE7RUErSTVDOztFQTlJRTtJQUFjLG1CQUFBO0VBa0poQjtBQUNGOztBQWhKQTtFQUNJO0lBQXVCLGdCQUFBO0VBbUp6QjtBQUNGOztBQWpKQTtFQUNJO0lBQXVCLGVBQUE7RUFvSnpCOztFQW5KRTtJQUF5QixXQUFBO0VBdUozQjtBQUNGOztBQXJKQTtFQUNJO0lBQWEsaUJBQUE7RUF3SmY7O0VBdkpFO0lBQXlCLFdBQUE7RUEySjNCOztFQTFKRTtJQUE2QixrQkFBQTtFQThKL0I7QUFDRjs7QUE1SkE7RUFDSTtJQUNJLGlCQUFBO0lBQ0Esa0JBQUE7SUFDQSxtQkFBQTtFQThKTjtBQUNGOztBQTNKQTtFQUNJO0lBQXNCLFlBQUE7RUE4SnhCOztFQTVKRTtJQUFxQixtQkFBQTtFQWdLdkI7O0VBL0pFO0lBQWlCLGFBQUE7RUFtS25COztFQWpLRTtJQUNJLGlCQUFBO0lBQ0Esa0JBQUE7RUFvS047O0VBaktFO0lBQ0ksVUFBQTtFQW9LTjs7RUFqS0U7SUFDSSxlQUFBO0lBQ0EsbUJBQUE7RUFvS047O0VBaktFO0lBQW9CLGdCQUFBO0VBcUt0Qjs7RUFwS0U7SUFBYSxnQkFBQTtFQXdLZjs7RUF2S0U7SUFBYSxtQkFBQTtFQTJLZjs7RUExS0U7SUFBZ0IsbUJBQUE7RUE4S2xCOztFQTVLRTtJQUNJLGdCQUFBO0lBQ0EsaUJBQUE7SUFDQSxrQkFBQTtFQStLTjs7RUE1S0U7SUFBZSxrQkFBQTtFQWdMakI7O0VBL0tFO0lBQXNCLGlCQUFBO0VBbUx4Qjs7RUFsTEU7SUFBOEIsZUFBQTtFQXNMaEM7O0VBcExEO0lBQ08sY0FBQTtJQUNBLG1CQUFBO0VBdUxOOztFQXJMRTtJQUNJLGNBQUE7SUFDQSxnQkFBQTtJQUNBLGtCQUFBO0VBd0xOOztFQXJMRTtJQUFtQixrQkFBQTtFQXlMckI7O0VBeExFO0lBQWMsa0JBQUE7RUE0TGhCOztFQTNMRTtJQUFrQixlQUFBO0VBK0xwQjs7RUE5TEU7SUFDSSxhQUFBO0lBQ0EsZUFBQTtFQWlNTjs7RUE5TEU7SUFDSSxhQUFBO0lBQ0EsZUFBQTtJQUNBLGVBQUE7RUFpTU47O0VBOUxFO0lBQ0ksY0FBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtFQWlNTjs7RUE5TEU7SUFBUyxXQUFBO0VBa01YO0FBQ0Y7O0FBaE1BO0VBQ0k7SUFBc0IsWUFBQTtFQW1NeEI7O0VBbE1FO0lBQWEsY0FBQTtFQXNNZjs7RUFyTUU7SUFBZ0IsbUJBQUE7RUF5TWxCOztFQXhNRTtJQUFnQixlQUFBO0VBNE1sQjs7RUEzTUU7SUFBdUIsaUJBQUE7RUErTXpCOztFQTlNRTtJQUNJLFdBQUE7SUFDQSxhQUFBO0lBQ0EsdUJBQUE7SUFDQSw2QkFBQTtJQUNBLDRCQUFBO0VBaU5OOztFQTlNRTtJQUNJLGlCQUFBO0lBQ0EsWUFBQTtFQWlOTjs7RUE5TUU7SUFDSSxnQkFBQTtJQUNBLG1CQUFBO0lBQ0EsV0FBQTtFQWlOTjs7RUE5TUU7SUFDSSx5QkFBQTtFQWlOTjs7RUE5TUU7SUFDSSxZQUFBO0VBaU5OOztFQTlNRTtJQUNJLFdBQUE7SUFDQSxtQkFBQTtJQUNBLGdCQUFBO0lBQ0EsZ0JBQUE7RUFpTk47O0VBOU1FO0lBQTRCLGlCQUFBO0VBa045Qjs7RUFqTkU7SUFBZSxrQkFBQTtFQXFOakI7O0VBcE5FO0lBQXNCLGVBQUE7RUF3TnhCOztFQXZORTtJQUE4QixpQkFBQTtFQTJOaEM7QUFDRjs7QUF6TkE7RUFDSTtJQUFhLGdCQUFBO0VBNE5mOztFQTNORTtJQUNJLFdBQUE7SUFDQSxtQkFBQTtFQThOTjs7RUEzTkU7SUFDSSxpQkFBQTtJQUNBLGtCQUFBO0VBOE5OOztFQTNORTtJQUNJLGdCQUFBO0lBQ0EsbUJBQUE7RUE4Tk47O0VBM05FOztJQUVJLGNBQUE7RUE4Tk47O0VBM05FO0lBQ0ksZ0JBQUE7RUE4Tk47O0VBM05FOztJQUVJLGtCQUFBO0VBOE5OO0FBQ0Y7O0FBM05BO0VBQ0k7SUFBUyxzQkFBQTtFQThOWDs7RUE3TkU7SUFBYyxnQkFBQTtFQWlPaEI7QUFDRjs7QUEvTkE7RUFDSTtJQUFzQixZQUFBO0VBa094Qjs7RUFqT0U7SUFBYSxlQUFBO0VBcU9mOztFQXBPRTtJQUFnQixxQkFBQTtFQXdPbEI7O0VBdk9FO0lBQWdCLGlCQUFBO0VBMk9sQjs7RUExT0U7SUFBdUIsaUJBQUE7RUE4T3pCO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9tb2R1bGVzL2dlbmVyYWwvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG5cbmJvZHkge1xuXHRmb250LWZhbWlseTogJ09wZW4gU2FucycsIEhlbHZldGljYSwgQXJpYWwsIHNhbnMtc2VyaWY7XG5cdGZvbnQtc2l6ZTogMTdweDtcbiAgICBmb250LXdlaWdodDogMzAwO1xuICAgIGNvbG9yOiAjNjY2NjY2O1xuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcbn1cblxuYSB7IHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2U7IH1cbmE6aG92ZXIsIGE6Zm9jdXMsIGEuYWN0aXZlIHtcbiAgICBjb2xvcjogIzU4ZDVmZjtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG5cbi50bS13ZWxjb21lLXNlY3Rpb24ge1xuICAgIGhlaWdodDogODN2aDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRTdBN0MwO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vLi4vLi4vLi4vYXNzZXRzL2ltZy9pbnNlcnRpb24taGVhZGVyLmpwZycpO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBtYXgtaGVpZ2h0OiA3MDBweDtcbiAgICBtaW4taGVpZ2h0OiA0MDBweDtcbn1cblxuLnRtLWZhLWJpZyB7IGZvbnQtc2l6ZTogNWVtOyB9XG4udG0tZmEtbWItYmlnIHsgbWFyZ2luLWJvdHRvbTogM3JlbTsgfVxuXG4udG0tbmF2YmFyLWNvbnRhaW5lciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbn1cblxuLm5hdmJhci1uYXYgeyBtYXJnaW4tcmlnaHQ6IDA7IH1cbi5uYXYtbGluayB7IGZvbnQtc2l6ZTogMS40cmVtOyB9XG5cbi50bS1zaXRlLWRlc2NyaXB0aW9uIHtcbiAgICBmb250LXNpemU6IDEuNnJlbTtcbiAgICBjb2xvcjogd2hpdGU7XG59XG5cbi5uYXZiYXItZXhwYW5kLXNtIC5uYXZiYXItbmF2IC5uYXYtbGluayB7IHBhZGRpbmc6IDEwcHggMjBweDsgfVxuLm5hdmJhci1uYXYgeyBmbGV4LWRpcmVjdGlvbjogcm93OyB9XG4udG0tc2l0ZS1uYW1lIHsgZm9udC1zaXplOiA0cmVtOyB9XG5cbi50bS1zZWFyY2gtZm9ybS1jb250YWluZXIge1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgYm9yZGVyOiA0cHggc29saWQgd2hpdGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2IzNWU2YztcbiAgICBtYXJnaW4tdG9wOiAtNTRweDtcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDBweCAwcHggNXB4IDBweCByZ2JhKDAsMCwwLDAuNzUpO1xuICAgIC1tb3otYm94LXNoYWRvdzogMHB4IDBweCA1cHggMHB4IHJnYmEoMCwwLDAsMC43NSk7XG4gICAgYm94LXNoYWRvdzogMHB4IDBweCA1cHggMHB4IHJnYmEoMCwwLDAsMC43NSk7XG59XG5cbi50bS1zZWFyY2gtZm9ybSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA5NnB4O1xuICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xufVxuXG4udG0tc2VhcmNoLWJveCB7XG4gICAgcGFkZGluZy1sZWZ0OiAyOHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDI4cHg7XG4gICAgd2lkdGg6IDYwJTtcbn1cblxuLnRtLWFkdmFuY2VkLWJveCB7IHdpZHRoOiAyMCU7IH1cblxuLmZvcm0taW5saW5lIC5mb3JtLWNvbnRyb2wudG0tc2VhcmNoLWlucHV0IHtcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG4gICAgcGFkZGluZzogMTBweCAxNXB4O1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB3aWR0aDogNzAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDA7XG59XG5cbi5mb3JtLWNvbnRyb2wudG0tc2VhcmNoLWlucHV0OjpwbGFjZWhvbGRlciB7IC8qIENocm9tZSwgRmlyZWZveCwgT3BlcmEsIFNhZmFyaSAxMC4xKyAqL1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIG9wYWNpdHk6IDE7IC8qIEZpcmVmb3ggKi9cbn1cblxuLmZvcm0tY29udHJvbC50bS1zZWFyY2gtaW5wdXQ6LW1zLWlucHV0LXBsYWNlaG9sZGVyIHsgLyogSW50ZXJuZXQgRXhwbG9yZXIgMTAtMTEgKi9cbiAgICBjb2xvcjogI2ZmZjtcbn1cblxuLmZvcm0tY29udHJvbC50bS1zZWFyY2gtaW5wdXQ6Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIE1pY3Jvc29mdCBFZGdlICovXG4gICAgY29sb3I6ICNmZmY7XG59XG5cbi5mb3JtLWNvbnRyb2w6OnBsYWNlaG9sZGVyIHsgLyogQ2hyb21lLCBGaXJlZm94LCBPcGVyYSwgU2FmYXJpIDEwLjErICovXG4gICAgY29sb3I6ICM2NjY2NjY7XG4gICAgb3BhY2l0eTogMTsgLyogRmlyZWZveCAqL1xufVxuXG4uZm9ybS1jb250cm9sOi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIEludGVybmV0IEV4cGxvcmVyIDEwLTExICovXG4gICAgY29sb3I6ICM2NjY2NjY7XG59XG5cbi5mb3JtLWNvbnRyb2w6Oi1tcy1pbnB1dC1wbGFjZWhvbGRlciB7IC8qIE1pY3Jvc29mdCBFZGdlICovXG4gICAgY29sb3I6ICM2NjY2NjY7XG59XG5cbi5mb3JtLWNvbnRyb2wge1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgYm9yZGVyLXJhZGl1czogMDtcbiAgICBwYWRkaW5nOiAxMnB4IDIwcHg7XG59XG5cbi5mb3JtLWNvbnRyb2w6Zm9jdXMge1xuICAgIGJvcmRlci1jb2xvcjogd2hpdGU7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbn1cblxuLnRtLXNlYXJjaC1zdWJtaXQge1xuICAgIHBhZGRpbmc6IDEwcHggMzVweDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgYm9yZGVyLXJhZGl1czogMDtcbn1cblxuLnRtLXNlYXJjaC1zdWJtaXQge1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlO1xufVxuXG4udG0tc2VhcmNoLXN1Ym1pdDpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgY29sb3I6ICNCMjVFNkQ7XG59XG5cbi50bS1hbGJ1bXMtY29udGFpbmVyIHtcbiAgICBtYXJnaW46IDgwcHggYXV0bztcbiAgICBtYXgtd2lkdGg6IDI2MHB4O1xufVxuXG4udG0tYWxidW0tY29sIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4udG0tbmV3LXJlbGVhc2Uge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvaW1nL2dyYWRpZW50LWJsdWUucG5nKTtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgY29sb3I6ICMxQzY4OUE7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDEwcHg7XG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDUwcHg7XG4gICAgcGFkZGluZy1yaWdodDogNTBweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICB3aWR0aDogMjAlO1xufVxuXG4vKi0tLS0tLS0tLS0tLS0tLSovXG4vKioqKiogU2FkaWUgKioqKiovXG4vKi0tLS0tLS0tLS0tLS0tLSovXG5cbi5ncmlkIGZpZ3VyZSB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uZ3JpZCBmaWd1cmUgaW1nIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWluLWhlaWdodDogMTAwJTtcbiAgICBtYXgtd2lkdGg6IDEwMCU7XG59XG5cbi5ncmlkIGZpZ3VyZSBmaWdjYXB0aW9uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHBhZGRpbmc6IDJlbTtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XG59XG5cbmZpZ3VyZS5lZmZlY3Qtc2FkaWUgZmlnY2FwdGlvbjo6YmVmb3JlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgcmdiYSg3Miw3Niw5NywwKSAwJSwgcmdiYSg3Miw3Niw5NywwLjgpIDc1JSk7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgcmdiYSg3Miw3Niw5NywwKSAwJSwgcmdiYSg3Miw3Niw5NywwLjgpIDc1JSk7XG4gICAgY29udGVudDogJyc7XG4gICAgb3BhY2l0eTogMDtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCw1MCUsMCk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLDUwJSwwKTtcbn1cblxuZmlndXJlLmVmZmVjdC1zYWRpZSBoMiB7XG4gICAgZm9udC1zaXplOiAxLjVyZW07XG4gICAgcGFkZGluZzogMS4ycmVtO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDEwJTtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG9wYWNpdHk6IDA7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAwLjM1cywgY29sb3IgMC4zNXM7XG4gICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuMzVzLCBjb2xvciAwLjM1cztcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwtODAlLDApO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwtODAlLDApO1xufVxuXG5maWd1cmUuZWZmZWN0LXNhZGllIGZpZ2NhcHRpb246OmJlZm9yZSxcbmZpZ3VyZS5lZmZlY3Qtc2FkaWUgcCB7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBvcGFjaXR5IDAuMzVzLCAtd2Via2l0LXRyYW5zZm9ybSAwLjM1cztcbiAgICB0cmFuc2l0aW9uOiBvcGFjaXR5IDAuMzVzLCB0cmFuc2Zvcm0gMC4zNXM7XG59XG5cbmZpZ3VyZS5lZmZlY3Qtc2FkaWUgcCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJvdHRvbTogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHBhZGRpbmc6IDEuMnJlbTtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG9wYWNpdHk6IDA7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsMTBweCwwKTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsMTBweCwwKTtcbn1cblxuZmlndXJlLmVmZmVjdC1zYWRpZTpob3ZlciBoMiB7XG4gICAgb3BhY2l0eTogMTtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwtNTAlLDApIHRyYW5zbGF0ZTNkKDAsLTEwcHgsMCk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLC01MCUsMCkgdHJhbnNsYXRlM2QoMCwtMTBweCwwKTtcbn1cblxuZmlndXJlLmVmZmVjdC1zYWRpZTpob3ZlciBmaWdjYXB0aW9uOjpiZWZvcmUgLFxuZmlndXJlLmVmZmVjdC1zYWRpZTpob3ZlciBwIHtcbiAgICBvcGFjaXR5OiAxO1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLDAsMCk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLDAsMCk7XG59XG5cbi50bS10YWctbGluZSB7XG4gICAgbWFyZ2luLWJvdHRvbTogNjBweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWluLWhlaWdodDogMjg1cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvaW1nL2luc2VydGlvbi0xODAweDQ1MC5qcGcpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cblxuLnRtLXRhZy1saW5lLXRpdGxlIHtcbiAgICBmb250LXNpemU6IDJyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBmb250LXdlaWdodDogMzAwO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4udG0tZm9udC0zMDAgeyBmb250LXdlaWdodDogMzAwOyB9XG4udG0tdGV4dC1ncmF5LCAudG0tbGluay1ncmF5IHsgY29sb3I6ICM2NjY2NjY7IH1cbi50bS10ZXh0LXdoaXRlIHsgY29sb3I6ICNmZmY7IH1cbi50bS10ZXh0LWJsdWUgeyBjb2xvcjogIzZlOWVjZTsgfVxuLnRtLXRleHQtYmx1ZS1kYXJrIHsgY29sb3I6ICMwMDY2OTk7IH1cbi50bS10ZXh0LXBpbmsgeyBjb2xvcjogI0Q2Njg4RjsgfVxuLnRtLXRleHQtcGluay1kYXJrIHsgY29sb3I6ICNDQzY2OTk7IH1cbi50bS10ZXh0LWJyb3duIHsgY29sb3I6ICNDQzY2NjY7IH1cbi50bS1iZy1ncmF5IHsgYmFja2dyb3VuZC1jb2xvcjogI0YyRjJGMjsgfVxuLnRtLWJnLXBpbmsgeyBiYWNrZ3JvdW5kLWNvbG9yOiAjZGU3MDk5OyB9XG4udG0tYmctcGluay1saWdodCB7IGJhY2tncm91bmQtY29sb3I6ICNmOWUzZWI7IH1cbi50bS1iZy1waW5rLWxpZ2h0LTIgeyBiYWNrZ3JvdW5kLWNvbG9yOiAjRURBQUJDOyB9XG4udG0tYmctYmx1ZSB7IGJhY2tncm91bmQtY29sb3I6ICM2Njk5Y2M7IH1cblxuLm1lZGlhLWJveGVzIHtcbiAgICBtYXgtd2lkdGg6IDkzMHB4O1xuICAgIG1hcmdpbjogMCBhdXRvO1xufVxuXG4ubWVkaWEgeyBtYXJnaW4tYm90dG9tOiAzNXB4OyB9XG4ubWVkaWEtYm9keSB7IGRpc3BsYXk6IGZsZXg7IH1cblxuLnRtLWJ1eS1ib3gge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICB3aWR0aDogMTQwcHg7XG59XG5cbi50bS1idXkge1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMjBweCA0NXB4O1xufVxuXG4udG0tYmctYmx1ZS50bS1idXk6aG92ZXIsXG4udG0tYmctYmx1ZS50bS1idXk6Zm9jdXMge1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMjc3YmNjO1xufVxuXG4udG0tYmctcGluay50bS1idXk6aG92ZXIsXG4udG0tYmctcGluay50bS1idXk6Zm9jdXMge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNkMTQ1Nzk7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG4udG0tZGVzY3JpcHRpb24tYm94IHsgcGFkZGluZzogMzBweCAzNXB4OyB9XG5cbi50bS1wcmljZS10YWcge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxLjJyZW07XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbn1cblxuLnRtLXN1YnNjcmliZS1mb3JtIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiA1MHB4O1xufVxuXG4udG0tc3Vic2NyaWJlLWlucHV0IHtcbiAgICB3aWR0aDogMzUwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjVweDtcbiAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgIGJvcmRlci1jb2xvcjogI0Q2Njg4RjtcbiAgICBwYWRkaW5nOiAxM3B4IDE1cHg7XG59XG5cbi5mb3JtLWNvbnRyb2wudG0tc3Vic2NyaWJlLWlucHV0OmZvY3VzIHsgYm9yZGVyLWNvbG9yOiAjZGY2Zjk4OyB9XG5cbi50bS1zdWJzY3JpYmUtYnRuIHtcbiAgICBib3JkZXI6IG5vbmU7XG4gICAgcGFkZGluZzogMTJweCA0MHB4O1xuICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2U7XG59XG5cbi50bS1zdWJzY3JpYmUtYnRuOmhvdmVyLFxuLnRtLXN1YnNjcmliZS1mb2N1cyB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2QxNDU3OTtcbn1cblxuLnRtLXN1YnNjcmliZS1pbWcge1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi8uLi8uLi9hc3NldHMvaW1nL2luc2VydGlvbi02MDB4NDAwLmpwZyk7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIG1pbi1oZWlnaHQ6IDQwMHB4O1xufVxuXG4udG0tYWJvdXQtaW1nIHtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL2ltZy9pbnNlcnRpb24tNTYweDMzNi0wMS5qcGcpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBoZWlnaHQ6IDMzNnB4O1xuICAgIG1heC13aWR0aDogNTYwcHg7XG59XG5cbi50bS1tYi0zMCB7IG1hcmdpbi1ib3R0b206IDMwcHg7IH1cbi50bS1tYi1tZWRpdW0geyBtYXJnaW4tYm90dG9tOiA5MHB4OyB9XG4udG0tbWItYmlnIHsgbWFyZ2luLWJvdHRvbTogMTMwcHg7IH1cbi50bS1tdC1iaWcgeyBtYXJnaW4tdG9wOiAxMDBweDsgfVxuXG4udG0tYWJvdXQtaDIge1xuICAgIGZvbnQtc2l6ZTogMS40cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG59XG5cbi50bS12LWNlbnRlciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG4udG0tbWVkaWEtdi1jZW50ZXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAzM3B4IDMwcHg7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2U7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDBweDtcbn1cblxuLm5hdi10YWJzIC5uYXYtaXRlbSB7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLm5hdi10YWJzIC5uYXYtaXRlbTpsYXN0LWNoaWxkIHsgbWFyZ2luLWJvdHRvbTogMDsgfVxuLnRtLW1lZGlhLWxpbmsgeyBmb250LXNpemU6IDEuMnJlbTsgfVxuXG4udG0tYWJvdXQtcm93IHtcbiAgICBtYXgtd2lkdGg6IDExOTBweDtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG59XG5cbi50bS1hYm91dC10ZXh0IHtcbiAgICBtYXgtd2lkdGg6IDU2MHB4O1xuICAgIHBhZGRpbmc6IDAgNTBweDtcbn1cblxuLnRtLWFib3V0LWRlc2NyaXB0aW9uIHsgbGluZS1oZWlnaHQ6IDEuODsgfVxuXG4udG0tbWVkaWEtMiB7XG4gICAgcGFkZGluZzogNTVweDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4udG0tbWVkaWEtYm9keS0yIHsgZmxleC1kaXJlY3Rpb246IGNvbHVtbjsgfVxuLnRtLW1lZGlhLWJvZHktMiAuYnRuIHsgYWxpZ24tc2VsZjogZmxleC1lbmQ7IH1cblxuLnRtLW1lZGlhLTItaWNvbiB7XG4gICAgbWFyZ2luLXJpZ2h0OiA2MHB4O1xuICAgIGFsaWduLXNlbGY6IGZsZXgtc3RhcnQ7XG59XG5cbi50bS1tZWRpYS0yLWhlYWRlciB7XG4gICAgZm9udC1zaXplOiAxLjRyZW07XG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcbn1cblxuLmJ0bi1zZWNvbmRhcnkge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICMzM0NDRkY7XG4gICAgYm9yZGVyLWNvbG9yOiAjMzNDQ0ZGO1xuICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xuICAgIHBhZGRpbmc6IDEycHggNDBweDtcbn1cblxuLmJ0bi1zZWNvbmRhcnk6Zm9jdXMsXG4uYnRuLXNlY29uZGFyeTpob3ZlciB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzI2OWRjNDtcbiAgICBib3JkZXItY29sb3I6ICMyNjlkYzQ7XG59XG5cbi5uYXYtdGFicyAubmF2LWxpbmsge1xuICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgYm9yZGVyOiBub25lO1xuICAgIGNvbG9yOiAjNjY2NjY2O1xufVxuXG4ubmF2LXRhYnMgeyBib3JkZXItYm90dG9tOiAwOyB9XG5cbi5uYXYtdGFicyAubmF2LWl0ZW0uc2hvdyAubmF2LWxpbmssXG4ubmF2LXRhYnMgLm5hdi1saW5rLmFjdGl2ZSxcbi5uYXYtdGFicyAubmF2LWxpbms6aG92ZXIge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNFREFBQkM7XG4gICAgY29sb3I6IHdoaXRlO1xufVxuXG4udG0tdGFiLWxpbmtzLWNvbnRhaW5lcixcbi50bS10YWItY29udGVudC1jb250YWluZXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtaW4taGVpZ2h0OiAxcHg7XG59XG5cbi50bS10YWItbGlua3MtY29udGFpbmVyIHtcbiAgICBmbGV4OiAwIDAgMjglO1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xufVxuXG4udG0tdGFiLWNvbnRlbnQtY29udGFpbmVyIHtcbiAgICBmbGV4OiAwIDAgNzIlO1xuICAgIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xufVxuXG4udGFiLWNvbnRlbnQge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLnRtLWNvbnRhY3QtY29sIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcbn1cblxuLnRtLWNvbnRhY3QtbGVmdCB7XG4gICAgZmxleDogMCAwIDIwJTtcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG5cbi50bS1jb250YWN0LW1pZGRsZSB7XG4gICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xuICAgIHBhZGRpbmc6IDQ1cHggNTBweDtcbn1cblxuLnRtLWNvbnRhY3QtcmlnaHQgeyBwYWRkaW5nOiA0NXB4IDUwcHg7IH1cblxuLnRtLWNvbnRhY3QtbWlkZGxlLFxuLnRtLWNvbnRhY3QtcmlnaHQge1xuICAgIGZsZXg6IDAgMCAzOCU7XG4gICAgbWF4LXdpZHRoOiA0NDBweDtcbn1cblxuLnRtLXNlbGVjdCB7XG4gICAgYm9yZGVyLXJhZGl1czogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAxMnB4IDIwcHg7XG59XG5cbi50bS1zZWxlY3QtZ3JvdXAge1xuICAgIHBhZGRpbmc6IDhweCAwO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xufVxuXG4udG0tcmFkaW8tbGFiZWwgeyBjdXJzb3I6IHBvaW50ZXI7IH1cbi50bS1yYWRpby1sYWJlbDpsYXN0LWNoaWxkIHsgbWFyZ2luLXJpZ2h0OiAxNXB4OyB9XG5pZnJhbWUgeyBtYXgtd2lkdGg6IDEwMCU7IH1cbiNnb29nbGUtbWFwIHsgYm9yZGVyOiAxcHggc29saWQgI2ZmZjsgfVxuXG4vKiBMb2FkZXIgKi9cbiNsb2FkZXItd3JhcHBlciB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB6LWluZGV4OiAyMDAwO1xufVxuI2xvYWRlciB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0b3A6IDUwJTtcbiAgICB3aWR0aDogMTUwcHg7XG4gICAgaGVpZ2h0OiAxNTBweDtcbiAgICBtYXJnaW46IC03NXB4IDAgMCAtNzVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgYm9yZGVyOiAzcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyLXRvcC1jb2xvcjogIzM0OThkYjtcblxuICAgIC13ZWJraXQtYW5pbWF0aW9uOiBzcGluIDJzIGxpbmVhciBpbmZpbml0ZTsgLyogQ2hyb21lLCBPcGVyYSAxNSssIFNhZmFyaSA1KyAqL1xuICAgIGFuaW1hdGlvbjogc3BpbiAycyBsaW5lYXIgaW5maW5pdGU7IC8qIENocm9tZSwgRmlyZWZveCAxNissIElFIDEwKywgT3BlcmEgKi9cblxuICAgIHotaW5kZXg6IDEwMDE7XG59XG5cbiNsb2FkZXI6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDVweDtcbiAgICBsZWZ0OiA1cHg7XG4gICAgcmlnaHQ6IDVweDtcbiAgICBib3R0b206IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgYm9yZGVyOiAzcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyLXRvcC1jb2xvcjogI2U3NGMzYztcblxuICAgIC13ZWJraXQtYW5pbWF0aW9uOiBzcGluIDNzIGxpbmVhciBpbmZpbml0ZTsgLyogQ2hyb21lLCBPcGVyYSAxNSssIFNhZmFyaSA1KyAqL1xuICAgIGFuaW1hdGlvbjogc3BpbiAzcyBsaW5lYXIgaW5maW5pdGU7IC8qIENocm9tZSwgRmlyZWZveCAxNissIElFIDEwKywgT3BlcmEgKi9cbn1cblxuI2xvYWRlcjphZnRlciB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAxNXB4O1xuICAgIGxlZnQ6IDE1cHg7XG4gICAgcmlnaHQ6IDE1cHg7XG4gICAgYm90dG9tOiAxNXB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBib3JkZXI6IDNweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItdG9wLWNvbG9yOiAjZjljOTIyO1xuXG4gICAgLXdlYmtpdC1hbmltYXRpb246IHNwaW4gMS41cyBsaW5lYXIgaW5maW5pdGU7IC8qIENocm9tZSwgT3BlcmEgMTUrLCBTYWZhcmkgNSsgKi9cbiAgICAgIGFuaW1hdGlvbjogc3BpbiAxLjVzIGxpbmVhciBpbmZpbml0ZTsgLyogQ2hyb21lLCBGaXJlZm94IDE2KywgSUUgMTArLCBPcGVyYSAqL1xufVxuXG5ALXdlYmtpdC1rZXlmcmFtZXMgc3BpbiB7XG4gICAgMCUgICB7XG4gICAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7ICAvKiBDaHJvbWUsIE9wZXJhIDE1KywgU2FmYXJpIDMuMSsgKi9cbiAgICAgICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDBkZWcpOyAgLyogSUUgOSAqL1xuICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTsgIC8qIEZpcmVmb3ggMTYrLCBJRSAxMCssIE9wZXJhICovXG4gICAgfVxuICAgIDEwMCUge1xuICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7ICAvKiBDaHJvbWUsIE9wZXJhIDE1KywgU2FmYXJpIDMuMSsgKi9cbiAgICAgICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7ICAvKiBJRSA5ICovXG4gICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7ICAvKiBGaXJlZm94IDE2KywgSUUgMTArLCBPcGVyYSAqL1xuICAgIH1cbn1cbkBrZXlmcmFtZXMgc3BpbiB7XG4gICAgMCUgICB7XG4gICAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7ICAvKiBDaHJvbWUsIE9wZXJhIDE1KywgU2FmYXJpIDMuMSsgKi9cbiAgICAgICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDBkZWcpOyAgLyogSUUgOSAqL1xuICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTsgIC8qIEZpcmVmb3ggMTYrLCBJRSAxMCssIE9wZXJhICovXG4gICAgfVxuICAgIDEwMCUge1xuICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7ICAvKiBDaHJvbWUsIE9wZXJhIDE1KywgU2FmYXJpIDMuMSsgKi9cbiAgICAgICAgLW1zLXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7ICAvKiBJRSA5ICovXG4gICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7ICAvKiBGaXJlZm94IDE2KywgSUUgMTArLCBPcGVyYSAqL1xuICAgIH1cbn1cblxuI2xvYWRlci13cmFwcGVyIC5sb2FkZXItc2VjdGlvbiB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHRvcDogMDtcbiAgICB3aWR0aDogNTElO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiAjMjIyMjIyO1xuICAgIHotaW5kZXg6IDEwMDA7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoMCk7ICAvKiBDaHJvbWUsIE9wZXJhIDE1KywgU2FmYXJpIDMuMSsgKi9cbiAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDApOyAgLyogSUUgOSAqL1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgwKTsgIC8qIEZpcmVmb3ggMTYrLCBJRSAxMCssIE9wZXJhICovXG59XG5cbiNsb2FkZXItd3JhcHBlciAubG9hZGVyLXNlY3Rpb24uc2VjdGlvbi1sZWZ0IHtcbiAgICBsZWZ0OiAwO1xufVxuXG4jbG9hZGVyLXdyYXBwZXIgLmxvYWRlci1zZWN0aW9uLnNlY3Rpb24tcmlnaHQge1xuICAgIHJpZ2h0OiAwO1xufVxuXG4vKiBMb2FkZWQgKi9cbi5sb2FkZWQgI2xvYWRlci13cmFwcGVyIC5sb2FkZXItc2VjdGlvbi5zZWN0aW9uLWxlZnQge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xMDAlKTsgIC8qIENocm9tZSwgT3BlcmEgMTUrLCBTYWZhcmkgMy4xKyAqL1xuICAgICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC0xMDAlKTsgIC8qIElFIDkgKi9cbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtMTAwJSk7ICAvKiBGaXJlZm94IDE2KywgSUUgMTArLCBPcGVyYSAqL1xuXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC43cyAwLjNzIGN1YmljLWJlemllcigwLjY0NSwgMC4wNDUsIDAuMzU1LCAxLjAwMCk7XG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC43cyAwLjNzIGN1YmljLWJlemllcigwLjY0NSwgMC4wNDUsIDAuMzU1LCAxLjAwMCk7XG59XG5cbi5sb2FkZWQgI2xvYWRlci13cmFwcGVyIC5sb2FkZXItc2VjdGlvbi5zZWN0aW9uLXJpZ2h0IHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgxMDAlKTsgIC8qIENocm9tZSwgT3BlcmEgMTUrLCBTYWZhcmkgMy4xKyAqL1xuICAgICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDEwMCUpOyAgLyogSUUgOSAqL1xuICAgICAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDEwMCUpOyAgLyogRmlyZWZveCAxNissIElFIDEwKywgT3BlcmEgKi9cblxuLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC43cyAwLjNzIGN1YmljLWJlemllcigwLjY0NSwgMC4wNDUsIDAuMzU1LCAxLjAwMCk7XG4gICAgdHJhbnNpdGlvbjogYWxsIDAuN3MgMC4zcyBjdWJpYy1iZXppZXIoMC42NDUsIDAuMDQ1LCAwLjM1NSwgMS4wMDApO1xufVxuXG4ubG9hZGVkICNsb2FkZXIge1xuICAgIG9wYWNpdHk6IDA7XG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLW91dDtcbiAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2Utb3V0O1xufVxuLmxvYWRlZCAjbG9hZGVyLXdyYXBwZXIge1xuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcblxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC0xMDAlKTsgIC8qIENocm9tZSwgT3BlcmEgMTUrLCBTYWZhcmkgMy4xKyAqL1xuICAgICAgICAtbXMtdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC0xMDAlKTsgIC8qIElFIDkgKi9cbiAgICAgICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtMTAwJSk7ICAvKiBGaXJlZm94IDE2KywgSUUgMTArLCBPcGVyYSAqL1xuXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMC4zcyAxcyBlYXNlLW91dDtcbiAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIDFzIGVhc2Utb3V0O1xufVxuXG4vKiBKYXZhU2NyaXB0IFR1cm5lZCBPZmYgKi9cbi5uby1qcyAjbG9hZGVyLXdyYXBwZXIgeyBkaXNwbGF5OiBub25lOyB9XG5cbkBtZWRpYSAobWluLXdpZHRoOiA1NzZweCkge1xuICAgIC50bS1hbGJ1bXMtY29udGFpbmVyIHsgbWF4LXdpZHRoOiBub25lOyB9XG4gICAgLm5hdmJhci1leHBhbmQtc20gLm5hdmJhci1uYXYgLm5hdi1saW5rIHsgcGFkZGluZzogMjBweCA0MHB4OyB9XG4gICAgLm5hdmJhci1uYXYgeyBtYXJnaW4tcmlnaHQ6IC00MHB4OyB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAgIC50bS1hbGJ1bXMtY29udGFpbmVyIHsgbWF4LXdpZHRoOiA1NDBweDsgfVxufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogOTkycHgpe1xuICAgIC50bS1hbGJ1bXMtY29udGFpbmVyIHsgbWF4LXdpZHRoOiBub25lOyB9XG4gICAgZmlndXJlLmVmZmVjdC1zYWRpZSBoMiB7IGJvdHRvbTogMjAlOyB9XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiAxMjAwcHgpe1xuICAgIC5jb250YWluZXIgeyBtYXgtd2lkdGg6IDEyMzBweDsgfVxuICAgIGZpZ3VyZS5lZmZlY3Qtc2FkaWUgaDIgeyBib3R0b206IDEwJTsgfVxuICAgIC50bS1yYWRpby1sYWJlbDpsYXN0LWNoaWxkIHsgbWFyZ2luLXJpZ2h0OiAzMHB4OyB9XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiAxMTk5cHgpIHtcbiAgICAudG0tbmV3LXJlbGVhc2Uge1xuICAgICAgICBmb250LXNpemU6IDEuMXJlbTtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAzNXB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAzNXB4O1xuICAgIH1cbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDk5MXB4KSB7XG4gICAgLnRtLXdlbGNvbWUtc2VjdGlvbiB7IGhlaWdodDogNzB2aDsgfVxuXG4gICAgLnRtLWFib3V0LWNvbC1sZWZ0IHsgbWFyZ2luLWJvdHRvbTogMjBweDsgfVxuICAgIC50bS1hYm91dC10ZXh0IHsgcGFkZGluZzogNTBweDsgfVxuXG4gICAgLnRtLWFib3V0LXRleHQsIC50bS1hYm91dC1pbWcge1xuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgIH1cblxuICAgIC5mb3JtLWlubGluZSAuZm9ybS1jb250cm9sLnRtLXNlYXJjaC1pbnB1dCB7XG4gICAgICAgIHdpZHRoOiA2MCU7XG4gICAgfVxuXG4gICAgLnRtLWFkdmFuY2VkLWJveCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgcGFkZGluZy1yaWdodDogMzBweDtcbiAgICB9XG5cbiAgICAudG0tc3Vic2NyaWJlLWltZyB7IHBhZGRpbmctcmlnaHQ6IDA7IH1cbiAgICAudG0tbXQtYmlnIHsgbWFyZ2luLXRvcDogNjBweDsgfVxuICAgIC50bS1tYi1iaWcgeyBtYXJnaW4tYm90dG9tOiA4MHB4OyB9XG4gICAgLnRtLW1iLW1lZGl1bSB7IG1hcmdpbi1ib3R0b206IDQwcHg7IH1cblxuICAgIC50bS1zdWJzY3JpYmUtcm93IHtcbiAgICAgICAgbWF4LXdpZHRoOiA2MDBweDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICB9XG5cbiAgICAudG0tdGFiLWxpbmsgeyBwYWRkaW5nOiAxNXB4IDI1cHg7IH1cbiAgICAudG0tdGFiLWxpbmsgLmZhLTJ4IHsgZm9udC1zaXplOiAxLjhyZW07IH1cbiAgICAudG0tdGFiLWxpbmsgLnRtLW1lZGlhLWxpbmsgeyBmb250LXNpemU6IDFyZW07IH1cblx0XG5cdC50bS10YWItbGlua3MtY29udGFpbmVyIHtcbiAgICAgICAgZmxleDogMCAwIDEwMCU7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG4gICAgfVxuICAgIC50bS10YWItY29udGVudC1jb250YWluZXIge1xuICAgICAgICBmbGV4OiAwIDAgMTAwJTtcbiAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICAgIH1cblx0XG4gICAgLnRtLW1lZGlhLTItaWNvbiB7IG1hcmdpbi1yaWdodDogMzBweDsgfVxuICAgIC50bS1tZWRpYS0yIHsgcGFkZGluZzogMzVweCAzMHB4OyB9XG4gICAgLnRtLWNvbnRhY3QtY29sIHsgZmxleC13cmFwOiB3cmFwOyB9XG4gICAgLnRtLWNvbnRhY3QtbGVmdCB7XG4gICAgICAgIGZsZXg6IDAgMCAzMCU7XG4gICAgICAgIG1hcmdpbi1yaWdodDogMDtcbiAgICB9XG5cbiAgICAudG0tY29udGFjdC1taWRkbGUge1xuICAgICAgICBmbGV4OiAwIDAgNjclO1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDA7XG4gICAgICAgIG1heC13aWR0aDogbm9uZTtcbiAgICB9XG5cbiAgICAudG0tY29udGFjdC1yaWdodCB7XG4gICAgICAgIGZsZXg6IDAgMCAxMDAlO1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICBtYXgtd2lkdGg6IG5vbmU7XG4gICAgfVxuXG4gICAgaWZyYW1lIHsgd2lkdGg6IDEwMCU7IH1cbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDc2N3B4KSB7XG4gICAgLnRtLXdlbGNvbWUtc2VjdGlvbiB7IGhlaWdodDogNjB2aDsgfVxuICAgIC50bS1mYS1iaWcgeyBmb250LXNpemU6IDRlbTsgfVxuICAgIC50bS1mYS1tYi1iaWcgeyBtYXJnaW4tYm90dG9tOiAycmVtOyB9XG4gICAgLnRtLXNpdGUtbmFtZSB7IGZvbnQtc2l6ZTogM3JlbTsgfVxuICAgIC50bS1zaXRlLWRlc2NyaXB0aW9uIHsgZm9udC1zaXplOiAxLjNyZW07IH1cbiAgICAudG0tbmV3LXJlbGVhc2Uge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgcGFkZGluZzogMjBweDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMHB4O1xuICAgICAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAwO1xuICAgIH1cblxuICAgIC50bS1zZWFyY2gtZm9ybSB7XG4gICAgICAgIGZsZXgtZmxvdzogY29sdW1uO1xuICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICAgfVxuXG4gICAgLmZvcm0taW5saW5lIC5mb3JtLWdyb3VwLnRtLXNlYXJjaC1ib3gge1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICB9XG5cbiAgICAuZm9ybS1pbmxpbmUgLmZvcm0tY29udHJvbC50bS1zZWFyY2gtaW5wdXQge1xuICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gMTQxcHgpO1xuICAgIH1cblxuICAgIC5mb3JtLWlubGluZSAuZm9ybS1jb250cm9sLnRtLXNlYXJjaC1zdWJtaXQge1xuICAgICAgICB3aWR0aDogMTIxcHg7XG4gICAgfVxuXG4gICAgLmZvcm0taW5saW5lIC5mb3JtLWdyb3VwLnRtLWFkdmFuY2VkLWJveCB7XG4gICAgICAgIHdpZHRoOiBhdXRvO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwO1xuICAgIH1cblxuICAgIC50bS1zZWFyY2gtZm9ybS1jb250YWluZXIgeyBtYXJnaW4tdG9wOiAtNzBweDsgfVxuICAgIC50bS10YWItbGluayB7IHBhZGRpbmc6IDMzcHggMzBweDsgfVxuICAgIC50bS10YWItbGluayAuZmEtMnggeyBmb250LXNpemU6IDJyZW07IH1cbiAgICAudG0tdGFiLWxpbmsgLnRtLW1lZGlhLWxpbmsgeyBmb250LXNpemU6IDEuNHJlbTsgfVxufVxuXG5AbWVkaWEgKG1heC13aWR0aDogNTc1cHgpIHtcbiAgICAuY29udGFpbmVyIHsgbWF4LXdpZHRoOiA1NDBweDsgfVxuICAgIC5mb3JtLWlubGluZSAuZm9ybS1jb250cm9sLnRtLXNlYXJjaC1pbnB1dCB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgIH1cblxuICAgIC50bS1zZWFyY2gtc3VibWl0IHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICB9XG5cbiAgICAudG0tYWxidW1zLWNvbnRhaW5lciB7XG4gICAgICAgIG1hcmdpbi10b3A6IDQwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDQwcHg7XG4gICAgfVxuXG4gICAgLnRtLWNvbnRhY3QtbGVmdCxcbiAgICAudG0tY29udGFjdC1taWRkbGUge1xuICAgICAgICBmbGV4OiAwIDAgMTAwJTtcbiAgICB9XG5cbiAgICAudG0tY29udGFjdC1taWRkbGUge1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIH1cblxuICAgIC50bS1jb250YWN0LW1pZGRsZSxcbiAgICAudG0tY29udGFjdC1yaWdodCB7XG4gICAgICAgIHBhZGRpbmc6IDM1cHggMzBweDtcbiAgICB9XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA1MDBweCkge1xuICAgIC5tZWRpYSB7IGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47IH1cbiAgICAubWVkaWEtYm9keSB7IG1hcmdpbi10b3A6IDE1cHg7IH1cbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6IDQyMHB4KSB7XG4gICAgLnRtLXN1YnNjcmliZS1pbnB1dCB7IHdpZHRoOiAyNjBweDsgfVxuICAgIC50bS1mYS1iaWcgeyBmb250LXNpemU6IDNyZW07IH1cbiAgICAudG0tZmEtbWItYmlnIHsgbWFyZ2luLWJvdHRvbTogMS41cmVtOyB9XG4gICAgLnRtLXNpdGUtbmFtZSB7IGZvbnQtc2l6ZTogMi40cmVtOyB9XG4gICAgLnRtLXNpdGUtZGVzY3JpcHRpb24geyBmb250LXNpemU6IDEuMnJlbTsgfVxufSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.scss']
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/modules/general/not-found/not-found.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/modules/general/not-found/not-found.component.ts ***!
  \******************************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class NotFoundComponent {
    constructor() { }
    ngOnInit() {
    }
}
NotFoundComponent.ɵfac = function NotFoundComponent_Factory(t) { return new (t || NotFoundComponent)(); };
NotFoundComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NotFoundComponent, selectors: [["app-not-found"]], decls: 2, vars: 0, consts: [[1, "text-center"]], template: function NotFoundComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "not-found works!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21vZHVsZXMvZ2VuZXJhbC9ub3QtZm91bmQvbm90LWZvdW5kLmNvbXBvbmVudC5zY3NzIn0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NotFoundComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-not-found',
                templateUrl: './not-found.component.html',
                styleUrls: ['./not-found.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
const environment = {
    production: false,
    application: {
        name: 'angular-starter',
        angular: 'Angular 10.0.8',
        bootstrap: 'Bootstrap 4.5.1',
        fontawesome: 'Font Awesome 5.14.0',
    }
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/hounie/Documents/projects/catalogue_numerique/src/main/webapp/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map