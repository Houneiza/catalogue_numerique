package cm.maroua.univ.enspm.catalogue.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;


/**
 *
 * @author hounie
 */
@Entity
@Data
public class Auteur implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String nom;
     
    @JsonIgnore
    @XmlTransient
    @OneToMany(mappedBy = "auteur", fetch = FetchType.EAGER)
    private List<Livre> livres;

    public Auteur(String nom){
        this.nom = nom;
    }
    
    public Auteur(){
        
    }
}
