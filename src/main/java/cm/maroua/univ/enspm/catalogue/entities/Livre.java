package cm.maroua.univ.enspm.catalogue.entities;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import lombok.Data;

/**
 *
 * @author hounie
 */
@Entity
@Data
public class Livre implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String titre;

    @Column(nullable = false)

   private String cote;

    @Column(nullable = false, length = 1024*1024*30)
    byte[] file;
    
    @Transient
    String image;
    
    @Column(nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date anneeParution;
    
    @Column(nullable = false)
    private String maisonEdition;

    @ManyToOne
    private Categorie Categorie;

    @ManyToOne
    private Auteur auteur;
    
    
    public void updateImage(){
        this.image = new String(file);
        this.file = null;
    }


}
