package cm.maroua.univ.enspm.catalogue.dao;

import cm.maroua.univ.enspm.catalogue.entities.Auteur;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author hounie
 */
@Repository
public interface AuteurDao extends JpaRepository<Auteur, Long>{
    public Page<Auteur> findByNomLike(String string, PageRequest of);
    public Auteur findByNomLike(String string);
}
