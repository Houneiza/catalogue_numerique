
package cm.maroua.univ.enspm.catalogue.services;
import cm.maroua.univ.enspm.catalogue.entities.Auteur;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
/**
 *
 * @author hounie
 */
@Path("/auteurs")
public interface IAuteurRessource {
    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<Auteur> getAllAuteurs(@QueryParam("page") @DefaultValue("0") int page, @QueryParam("size") @DefaultValue("20") int size);
    
    @GET
    @Path("/search")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<Auteur> searchAuteurs(@QueryParam("nom")String nom, @DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int size);
    
    @GET
    @Path("{id: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Auteur> findAuteur(@PathParam("id") long id);
    
    @PUT
    //@Path("{id: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Auteur> updateAuteur(@QueryParam("id") long id,Auteur auteur);
    
    @POST
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Auteur> addAuteur(Auteur auteur);
    
    @DELETE
    @Path("{id: \\d+}")
    public void deleteAuteur(@PathParam("id") long id);
    
    
}
