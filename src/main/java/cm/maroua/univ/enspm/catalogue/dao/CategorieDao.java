package cm.maroua.univ.enspm.catalogue.dao;

import cm.maroua.univ.enspm.catalogue.entities.Categorie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author hounie
 */
@Repository
public interface CategorieDao extends JpaRepository<Categorie, Long>{
    public Page <Categorie> findBynameLike(String name,Pageable p);

    public Categorie findBynameLike(String name);
}
