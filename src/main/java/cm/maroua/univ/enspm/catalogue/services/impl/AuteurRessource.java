/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.maroua.univ.enspm.catalogue.services.impl;

import cm.maroua.univ.enspm.catalogue.dao.AuteurDao;
import cm.maroua.univ.enspm.catalogue.entities.Auteur;
import cm.maroua.univ.enspm.catalogue.services.IAuteurRessource;
import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author hounie
 */

public class AuteurRessource implements IAuteurRessource{
       @Autowired
       private AuteurDao auteurDao;
    @Override
    public Page<Auteur> getAllAuteurs(int page, int pagesize) {
        return auteurDao.findAll(PageRequest.of(page, pagesize));    }

    @Override
    public Page<Auteur> searchAuteurs(String nom, int page, int pagesize) {
        return auteurDao.findByNomLike("%" + nom + "%", PageRequest.of(page, pagesize));
    }

    @Override
    public ResponseEntity<Auteur> findAuteur(long id) {
        Optional<Auteur> auteur = auteurDao.findById(id);
        if (auteur.isPresent()) {
            return ResponseEntity.ok(auteur.get());
        }
        return ResponseEntity.notFound().build();    }

    @Override
    public ResponseEntity<Auteur> updateAuteur(long id, Auteur auteur) {
        return auteurDao.findById(id).map(
                c -> {
                    c.setNom(auteur.getNom());
                    c.setLivres(auteur.getLivres());
                    return ResponseEntity.ok(auteurDao.save(c));
                }
        ).orElse(
                ResponseEntity.notFound().build()
        );    }

    @Override
    public ResponseEntity<Auteur> addAuteur(Auteur auteur) {
         Auteur c = auteurDao.save(auteur);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(c.getId())
                .toUri();
        return ResponseEntity.created(location).body(c);    }

    @Override
    public void deleteAuteur(long id) {
         auteurDao.deleteById(id);    }
    
}
