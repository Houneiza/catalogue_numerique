package cm.maroua.univ.enspm.catalogue.dao;

import cm.maroua.univ.enspm.catalogue.entities.Utilisateur;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author hounie
 */
@Repository
public interface UtilisateurDao extends JpaRepository<Utilisateur, Long>{
     public List<Utilisateur> findByNomLike(String nom);
}
