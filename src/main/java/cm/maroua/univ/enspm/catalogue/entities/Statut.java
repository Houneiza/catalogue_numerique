/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.maroua.univ.enspm.catalogue.entities;

/**
 *
 * @author hounie
 */
public enum Statut {
    
  ADMIN("ADMIN"), USER("USER"), SUPERADMIN("SUPERADMIN");
    
    private String name;
    
    private Statut(String name){
        this.name = name;
    }
    
}
