/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.maroua.univ.enspm.catalogue.modelReponse;

import lombok.Data;

/**
 *
 * @author vaka
 */
@Data
public class GenericOutput<T> {
    
    private String message;
    
}
