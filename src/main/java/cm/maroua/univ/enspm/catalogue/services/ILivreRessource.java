
package cm.maroua.univ.enspm.catalogue.services;

import cm.maroua.univ.enspm.catalogue.entities.Auteur;
import cm.maroua.univ.enspm.catalogue.entities.Livre;
import cm.maroua.univ.enspm.catalogue.entities.Categorie;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 *
 * @author hounie
 */
@Path("/livres")
public interface ILivreRessource {
    
    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<Livre> getAllLivres(@DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
    
    @GET
    @Path("/search")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<Livre> searchLivresByTitre(@QueryParam("titre")String titre, @DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
   
    @GET
    @Path("/searchA")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<Livre> searchLivresByAuteurNomLike(@QueryParam("nom")String nom, @DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
    
    @GET
    @Path("/searchS")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<Livre> searchLivresByCategorieNameLike(@QueryParam("name")String name, @DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
   
    @GET
    @Path("{id: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Livre> findLivre(@PathParam("id") long id);
    
    @PUT
    @Path("{id: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Livre> updateLivre(@PathParam("id") long id,Livre livre);
    
    @POST
    @Produces(value ={MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Livre> addLivre(Livre livre);
    
    @DELETE
    @Path("{id: \\d+}")
    public void deleteLivre(@PathParam("id") long id);
    
    
}
