
package cm.maroua.univ.enspm.catalogue.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;
import javax.validation.constraints.NotNull;
/**
 *
 * @author hounie
 */
@Data
@Entity
public class Utilisateur implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    private String nom;
    
    @NotNull
    private String password;
    
    @Column
    private Statut status;
      
    
}
