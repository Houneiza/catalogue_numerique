/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.maroua.univ.enspm.catalogue.services.impl;
import cm.maroua.univ.enspm.catalogue.dao.CategorieDao;
import cm.maroua.univ.enspm.catalogue.entities.Categorie;
import cm.maroua.univ.enspm.catalogue.services.ICategorieRessource;
import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author hounie
 */

public class CategorieRessource implements ICategorieRessource{
    @Autowired
     private CategorieDao categorieDao;
    
    @Override
    public Page<Categorie> getAllCategories(int page, int pagesize) {
         return categorieDao.findAll(PageRequest.of(page, pagesize));    }
   
    @Override
    public Page<Categorie> searchCategories(String name, int page, int pagesize) {
        return categorieDao.findAll(PageRequest.of(page, pagesize));
    }

    @Override
    public ResponseEntity<Categorie> findCategorie(long id) {
       Optional<Categorie> categorie = categorieDao.findById(id);
        if (categorie.isPresent()) {
            return ResponseEntity.ok(categorie.get());
        }
        return ResponseEntity.notFound().build();    }

    @Override
    public ResponseEntity<Categorie> updateCategorie(long id, Categorie categorie) {
       return categorieDao.findById(id).map(
                c -> {
                    c.setName(categorie.getName());
                    return ResponseEntity.ok(categorieDao.save(c));
                }
        ).orElse(
                ResponseEntity.notFound().build()
        );    }

    @Override
    public ResponseEntity<Categorie> addCategorie(Categorie categorie) {
       Categorie c = categorieDao.save(categorie);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(c.getId())
                .toUri();
        return ResponseEntity.created(location).body(c);    }

    @Override
    public void deleteCategorie(long id) {
        categorieDao.deleteById(id);    }
 
    }
    

