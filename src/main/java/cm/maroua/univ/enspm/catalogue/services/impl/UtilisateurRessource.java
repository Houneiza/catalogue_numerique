package cm.maroua.univ.enspm.catalogue.services.impl;
import cm.maroua.univ.enspm.catalogue.dao.UtilisateurDao;
import cm.maroua.univ.enspm.catalogue.entities.Utilisateur;
import cm.maroua.univ.enspm.catalogue.services.IUtilisateurRessource;
import java.net.URI;
import static java.nio.file.Files.size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;



/**
 *
 * @author hounie
 */
public class UtilisateurRessource implements IUtilisateurRessource{
  @Autowired
    private UtilisateurDao utilisateurDao;
    
    @Override
    public ResponseEntity<Utilisateur> addUtilisateur(Utilisateur user) {
      Utilisateur u = utilisateurDao.save(user);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(u.getId())
                .toUri();
        return ResponseEntity.created(location).body(u);    }

    @Override
    public ResponseEntity<Utilisateur> findUtilisateur(long id) {
        return utilisateurDao.findById(id).map(
                u -> {
                    return ResponseEntity.ok(u);
                }
        ).orElse(ResponseEntity.notFound().build());
    }

    @Override
    public ResponseEntity<Utilisateur> updateUtilisateur(long id, Utilisateur user) {
       return utilisateurDao.findById(id).map(
                u -> {              
                    u.setNom(user.getNom());
                    u.setPassword(user.getPassword());
                    return ResponseEntity.ok(utilisateurDao.save(u));
                }
        ).orElse(ResponseEntity.notFound().build());    }

    @Override
    public void deleteUtilisateur(long id) {
          utilisateurDao.deleteById(id);    }

    @Override
    public ResponseEntity<Page<Utilisateur>> findAllUtilisateurs(int page, int size) {
        return ResponseEntity.ok(utilisateurDao.findAll(PageRequest.of(page, size)));
    }

    
  
    
}
