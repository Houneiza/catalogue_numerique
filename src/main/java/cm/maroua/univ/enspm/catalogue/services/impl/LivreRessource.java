/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.maroua.univ.enspm.catalogue.services.impl;

import cm.maroua.univ.enspm.catalogue.dao.AuteurDao;
import cm.maroua.univ.enspm.catalogue.dao.CategorieDao;
import cm.maroua.univ.enspm.catalogue.dao.LivreDao;
import cm.maroua.univ.enspm.catalogue.entities.Auteur;
import cm.maroua.univ.enspm.catalogue.entities.Categorie;
import cm.maroua.univ.enspm.catalogue.entities.Livre;
import cm.maroua.univ.enspm.catalogue.services.ILivreRessource;
import java.net.URI;
import java.util.Optional;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


/**
 *
 * @author hounie
 */

public class LivreRessource implements ILivreRessource{
     @Autowired
     private LivreDao livreDao; 
     @Autowired
     private AuteurDao auteurDao; 
     @Autowired
     private CategorieDao categorieDao; 

    @Override
    public Page<Livre> getAllLivres(int page, int pagesize) {
        Page<Livre> list = livreDao.findAll(PageRequest.of(page, pagesize));
        list.getContent().forEach((li) -> li.updateImage());
        return list;
                
    }

    @Override
    public Page<Livre> searchLivresByTitre(String titre, int page, int pagesize) {
        return livreDao.findByTitreLike("%" + titre + "%", PageRequest.of(page, pagesize));

    }

  @Override
    public Page<Livre> searchLivresByAuteurNomLike(String nom, int page, int pagesize) {
        return livreDao.findByAuteurNomLike("%" + nom+ "%", PageRequest.of(page, pagesize));
    }

    @Override
    public Page<Livre> searchLivresByCategorieNameLike(String name, int page, int pagesize) {
        return livreDao.findByCategorieNameLike("%" + name + "%", PageRequest.of(page, pagesize));
    }

    @Override
    public ResponseEntity<Livre> findLivre(long id) {
    Optional<Livre> livre = livreDao.findById(id);
        if (livre.isPresent()) {
        Livre liv = livre.get();
        liv.updateImage();
            return ResponseEntity.ok(liv);
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<Livre> updateLivre(long id, Livre livre) {
        return livreDao.findById(id).map(
                c -> {
                    c.setTitre(livre.getTitre());
                    c.setMaisonEdition(livre.getMaisonEdition());
                    c.setCote(livre.getCote());
                    c.setAnneeParution(livre.getAnneeParution());
                    
                    c.setFile(livre.getImage().getBytes());
                    //System.out.println("file C = "+c.getFile()+"\n\n image L = "+livre.getImage());
                    Auteur nom = auteurDao.findByNomLike(livre.getAuteur().getNom());
                    c.setAuteur(nom);
                    Categorie name = categorieDao.findBynameLike(livre.getCategorie().getName());
                    c.setCategorie(name);
                    return ResponseEntity.ok(livreDao.save(c));
                }
        ).orElse(
                ResponseEntity.notFound().build()
        );    }
    

    @Override 
    public ResponseEntity<Livre> addLivre(Livre livre) {
        livre.setFile(livre.getImage().getBytes());
        Auteur au = auteurDao.findByNomLike(livre.getAuteur().getNom());
        livre.setAuteur(au);
        Categorie c = categorieDao.findBynameLike(livre.getCategorie().getName());
        livre.setCategorie(c);
        Livre lv = livre;
        if(c != null && au != null){
             lv = livreDao.save(livre);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(c.getId())
                    .toUri();
            return ResponseEntity.created(location).body(lv);
        }
        return ResponseEntity.badRequest().body(lv);

    }

    
    @Override
    public void deleteLivre(long id) {
        livreDao.deleteById(id);    }

    
}
